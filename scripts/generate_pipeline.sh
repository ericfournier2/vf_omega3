export RAP_ID=vyt-470-aa
module load mugqic/mugqic_pipelines/2.2.0

PROJECT_BASE=/gs/project/vyt-470-aa/Omega3

PIPELINE_OUTPUT=$SCRATCH/Omega3/pipeline-output
mkdir -p $PIPELINE_OUTPUT
$MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.py -s '1-22' -l debug \
    -r $PROJECT_BASE/input/Readset.txt \
    -d $PROJECT_BASE/input/Design.txt \
    -o $PIPELINE_OUTPUT \
    --config  $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Mus_musculus.mm10.ini