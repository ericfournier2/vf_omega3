#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq PBSScheduler Job Submission Bash script
# Version: 2.2.0
# Created on: 2016-12-09T14:23:23
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 20 jobs
#   merge_trimmomatic_stats: 1 job
#   star: 42 jobs
#   picard_merge_sam_files: 0 job... skipping
#   picard_sort_sam: 20 jobs
#   picard_mark_duplicates: 20 jobs
#   picard_rna_metrics: 20 jobs
#   estimate_ribosomal_rna: 20 jobs
#   bam_hard_clip: 20 jobs
#   rnaseqc: 2 jobs
#   wiggle: 80 jobs
#   raw_counts: 20 jobs
#   raw_counts_metrics: 4 jobs
#   cufflinks: 20 jobs
#   cuffmerge: 1 job
#   cuffquant: 20 jobs
#   cuffdiff: 5 jobs
#   cuffnorm: 1 job
#   fpkm_correlation_matrix: 2 jobs
#   gq_seq_utils_exploratory_analysis_rnaseq: 3 jobs
#   differential_expression: 1 job
#   TOTAL: 322 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/gs/scratch/efournier/Omega3/pipeline-output
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.B11_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.B11_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.B11_RS.48273854d28d365203d0bfbda5687fc4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.B11_RS.48273854d28d365203d0bfbda5687fc4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/B11 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/B11_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/B11_R2.fastq.gz \
  trim/B11/B11_RS.trim.pair1.fastq.gz \
  trim/B11/B11_RS.trim.single1.fastq.gz \
  trim/B11/B11_RS.trim.pair2.fastq.gz \
  trim/B11/B11_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/B11/B11_RS.trim.log
trimmomatic.B11_RS.48273854d28d365203d0bfbda5687fc4.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.B12_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.B12_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.B12_RS.24ff527f66d11ebf492f6388b0a111ee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.B12_RS.24ff527f66d11ebf492f6388b0a111ee.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/B12 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/B12_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/B12_R2.fastq.gz \
  trim/B12/B12_RS.trim.pair1.fastq.gz \
  trim/B12/B12_RS.trim.single1.fastq.gz \
  trim/B12/B12_RS.trim.pair2.fastq.gz \
  trim/B12/B12_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/B12/B12_RS.trim.log
trimmomatic.B12_RS.24ff527f66d11ebf492f6388b0a111ee.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.B1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.B1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.B1_RS.953555c17d52be1a9261355ed9ca1164.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.B1_RS.953555c17d52be1a9261355ed9ca1164.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/B1 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/B1_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/B1_R2.fastq.gz \
  trim/B1/B1_RS.trim.pair1.fastq.gz \
  trim/B1/B1_RS.trim.single1.fastq.gz \
  trim/B1/B1_RS.trim.pair2.fastq.gz \
  trim/B1/B1_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/B1/B1_RS.trim.log
trimmomatic.B1_RS.953555c17d52be1a9261355ed9ca1164.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.B6_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.B6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.B6_RS.bcdad17d5c0be9611dee72fd5964f422.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.B6_RS.bcdad17d5c0be9611dee72fd5964f422.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/B6 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/B6_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/B6_R2.fastq.gz \
  trim/B6/B6_RS.trim.pair1.fastq.gz \
  trim/B6/B6_RS.trim.single1.fastq.gz \
  trim/B6/B6_RS.trim.pair2.fastq.gz \
  trim/B6/B6_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/B6/B6_RS.trim.log
trimmomatic.B6_RS.bcdad17d5c0be9611dee72fd5964f422.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.B8_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.B8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.B8_RS.7ae3e4cb0ba06f636602447717308e28.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.B8_RS.7ae3e4cb0ba06f636602447717308e28.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/B8 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/B8_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/B8_R2.fastq.gz \
  trim/B8/B8_RS.trim.pair1.fastq.gz \
  trim/B8/B8_RS.trim.single1.fastq.gz \
  trim/B8/B8_RS.trim.pair2.fastq.gz \
  trim/B8/B8_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/B8/B8_RS.trim.log
trimmomatic.B8_RS.7ae3e4cb0ba06f636602447717308e28.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.C11_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.C11_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.C11_RS.31f6d688203ea9dff8836360f37ccdc9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.C11_RS.31f6d688203ea9dff8836360f37ccdc9.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/C11 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/C11_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/C11_R2.fastq.gz \
  trim/C11/C11_RS.trim.pair1.fastq.gz \
  trim/C11/C11_RS.trim.single1.fastq.gz \
  trim/C11/C11_RS.trim.pair2.fastq.gz \
  trim/C11/C11_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/C11/C11_RS.trim.log
trimmomatic.C11_RS.31f6d688203ea9dff8836360f37ccdc9.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_7_JOB_ID: trimmomatic.C12_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.C12_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.C12_RS.c5d2d437860a1c5e6250fefa0a96100e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.C12_RS.c5d2d437860a1c5e6250fefa0a96100e.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/C12 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/C12_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/C12_R2.fastq.gz \
  trim/C12/C12_RS.trim.pair1.fastq.gz \
  trim/C12/C12_RS.trim.single1.fastq.gz \
  trim/C12/C12_RS.trim.pair2.fastq.gz \
  trim/C12/C12_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/C12/C12_RS.trim.log
trimmomatic.C12_RS.c5d2d437860a1c5e6250fefa0a96100e.mugqic.done
)
trimmomatic_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_8_JOB_ID: trimmomatic.C4_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.C4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.C4_RS.d84d195935a84c807d644c40eb1fb4a4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.C4_RS.d84d195935a84c807d644c40eb1fb4a4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/C4 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/C4_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/C4_R2.fastq.gz \
  trim/C4/C4_RS.trim.pair1.fastq.gz \
  trim/C4/C4_RS.trim.single1.fastq.gz \
  trim/C4/C4_RS.trim.pair2.fastq.gz \
  trim/C4/C4_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/C4/C4_RS.trim.log
trimmomatic.C4_RS.d84d195935a84c807d644c40eb1fb4a4.mugqic.done
)
trimmomatic_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_9_JOB_ID: trimmomatic.C5_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.C5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.C5_RS.6e419b1855dca9b0f0e0bddebdb17a5b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.C5_RS.6e419b1855dca9b0f0e0bddebdb17a5b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/C5 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/C5_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/C5_R2.fastq.gz \
  trim/C5/C5_RS.trim.pair1.fastq.gz \
  trim/C5/C5_RS.trim.single1.fastq.gz \
  trim/C5/C5_RS.trim.pair2.fastq.gz \
  trim/C5/C5_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/C5/C5_RS.trim.log
trimmomatic.C5_RS.6e419b1855dca9b0f0e0bddebdb17a5b.mugqic.done
)
trimmomatic_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_10_JOB_ID: trimmomatic.C9_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.C9_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.C9_RS.1aa488dd00a8978d29a4db80e0830da6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.C9_RS.1aa488dd00a8978d29a4db80e0830da6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/C9 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/C9_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/C9_R2.fastq.gz \
  trim/C9/C9_RS.trim.pair1.fastq.gz \
  trim/C9/C9_RS.trim.single1.fastq.gz \
  trim/C9/C9_RS.trim.pair2.fastq.gz \
  trim/C9/C9_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/C9/C9_RS.trim.log
trimmomatic.C9_RS.1aa488dd00a8978d29a4db80e0830da6.mugqic.done
)
trimmomatic_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_11_JOB_ID: trimmomatic.D12_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.D12_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.D12_RS.8c67488e2f53650cfd84f986c5603d1c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.D12_RS.8c67488e2f53650cfd84f986c5603d1c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/D12 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/D12_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/D12_R2.fastq.gz \
  trim/D12/D12_RS.trim.pair1.fastq.gz \
  trim/D12/D12_RS.trim.single1.fastq.gz \
  trim/D12/D12_RS.trim.pair2.fastq.gz \
  trim/D12/D12_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/D12/D12_RS.trim.log
trimmomatic.D12_RS.8c67488e2f53650cfd84f986c5603d1c.mugqic.done
)
trimmomatic_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_12_JOB_ID: trimmomatic.D2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.D2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.D2_RS.ccbef23ba2905df740aaea990daccce3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.D2_RS.ccbef23ba2905df740aaea990daccce3.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/D2 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/D2_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/D2_R2.fastq.gz \
  trim/D2/D2_RS.trim.pair1.fastq.gz \
  trim/D2/D2_RS.trim.single1.fastq.gz \
  trim/D2/D2_RS.trim.pair2.fastq.gz \
  trim/D2/D2_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/D2/D2_RS.trim.log
trimmomatic.D2_RS.ccbef23ba2905df740aaea990daccce3.mugqic.done
)
trimmomatic_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_13_JOB_ID: trimmomatic.D3_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.D3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.D3_RS.55608fab678892d68f79e9dece67063c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.D3_RS.55608fab678892d68f79e9dece67063c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/D3 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/D3_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/D3_R2.fastq.gz \
  trim/D3/D3_RS.trim.pair1.fastq.gz \
  trim/D3/D3_RS.trim.single1.fastq.gz \
  trim/D3/D3_RS.trim.pair2.fastq.gz \
  trim/D3/D3_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/D3/D3_RS.trim.log
trimmomatic.D3_RS.55608fab678892d68f79e9dece67063c.mugqic.done
)
trimmomatic_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_14_JOB_ID: trimmomatic.D4_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.D4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.D4_RS.ada4a41d38225834874c13e7a0e806eb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.D4_RS.ada4a41d38225834874c13e7a0e806eb.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/D4 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/D4_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/D4_R2.fastq.gz \
  trim/D4/D4_RS.trim.pair1.fastq.gz \
  trim/D4/D4_RS.trim.single1.fastq.gz \
  trim/D4/D4_RS.trim.pair2.fastq.gz \
  trim/D4/D4_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/D4/D4_RS.trim.log
trimmomatic.D4_RS.ada4a41d38225834874c13e7a0e806eb.mugqic.done
)
trimmomatic_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_15_JOB_ID: trimmomatic.D8_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.D8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.D8_RS.b972b48ba194b64d97d2b9302907d6f5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.D8_RS.b972b48ba194b64d97d2b9302907d6f5.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/D8 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/D8_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/D8_R2.fastq.gz \
  trim/D8/D8_RS.trim.pair1.fastq.gz \
  trim/D8/D8_RS.trim.single1.fastq.gz \
  trim/D8/D8_RS.trim.pair2.fastq.gz \
  trim/D8/D8_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/D8/D8_RS.trim.log
trimmomatic.D8_RS.b972b48ba194b64d97d2b9302907d6f5.mugqic.done
)
trimmomatic_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_16_JOB_ID: trimmomatic.E3_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.E3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.E3_RS.2c06e195864df07503ce5cc2df20119a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.E3_RS.2c06e195864df07503ce5cc2df20119a.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/E3 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/E3_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/E3_R2.fastq.gz \
  trim/E3/E3_RS.trim.pair1.fastq.gz \
  trim/E3/E3_RS.trim.single1.fastq.gz \
  trim/E3/E3_RS.trim.pair2.fastq.gz \
  trim/E3/E3_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/E3/E3_RS.trim.log
trimmomatic.E3_RS.2c06e195864df07503ce5cc2df20119a.mugqic.done
)
trimmomatic_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_17_JOB_ID: trimmomatic.E4_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.E4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.E4_RS.411220b0f629d0547156942f4cd2e8fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.E4_RS.411220b0f629d0547156942f4cd2e8fe.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/E4 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/E4_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/E4_R2.fastq.gz \
  trim/E4/E4_RS.trim.pair1.fastq.gz \
  trim/E4/E4_RS.trim.single1.fastq.gz \
  trim/E4/E4_RS.trim.pair2.fastq.gz \
  trim/E4/E4_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/E4/E4_RS.trim.log
trimmomatic.E4_RS.411220b0f629d0547156942f4cd2e8fe.mugqic.done
)
trimmomatic_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_18_JOB_ID: trimmomatic.E6_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.E6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.E6_RS.bd4dffc8a888c7d275a2de4ec85f4cfe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.E6_RS.bd4dffc8a888c7d275a2de4ec85f4cfe.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/E6 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/E6_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/E6_R2.fastq.gz \
  trim/E6/E6_RS.trim.pair1.fastq.gz \
  trim/E6/E6_RS.trim.single1.fastq.gz \
  trim/E6/E6_RS.trim.pair2.fastq.gz \
  trim/E6/E6_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/E6/E6_RS.trim.log
trimmomatic.E6_RS.bd4dffc8a888c7d275a2de4ec85f4cfe.mugqic.done
)
trimmomatic_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_19_JOB_ID: trimmomatic.E8_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.E8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.E8_RS.c6d89d6b2029de7adc14acec38982631.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.E8_RS.c6d89d6b2029de7adc14acec38982631.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/E8 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/E8_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/E8_R2.fastq.gz \
  trim/E8/E8_RS.trim.pair1.fastq.gz \
  trim/E8/E8_RS.trim.single1.fastq.gz \
  trim/E8/E8_RS.trim.pair2.fastq.gz \
  trim/E8/E8_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/E8/E8_RS.trim.log
trimmomatic.E8_RS.c6d89d6b2029de7adc14acec38982631.mugqic.done
)
trimmomatic_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_20_JOB_ID: trimmomatic.E9_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.E9_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.E9_RS.0ee4d2ecc670cda84aa8043ba7fe028b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.E9_RS.0ee4d2ecc670cda84aa8043ba7fe028b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/E9 && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /gs/project/vyt-470-aa/Omega3/input/E9_R1.fastq.gz \
  /gs/project/vyt-470-aa/Omega3/input/E9_R2.fastq.gz \
  trim/E9/E9_RS.trim.pair1.fastq.gz \
  trim/E9/E9_RS.trim.single1.fastq.gz \
  trim/E9/E9_RS.trim.pair2.fastq.gz \
  trim/E9/E9_RS.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/E9/E9_RS.trim.log
trimmomatic.E9_RS.0ee4d2ecc670cda84aa8043ba7fe028b.mugqic.done
)
trimmomatic_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=1700m | grep "[0-9]")
echo "$trimmomatic_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID:$trimmomatic_7_JOB_ID:$trimmomatic_8_JOB_ID:$trimmomatic_9_JOB_ID:$trimmomatic_10_JOB_ID:$trimmomatic_11_JOB_ID:$trimmomatic_12_JOB_ID:$trimmomatic_13_JOB_ID:$trimmomatic_14_JOB_ID:$trimmomatic_15_JOB_ID:$trimmomatic_16_JOB_ID:$trimmomatic_17_JOB_ID:$trimmomatic_18_JOB_ID:$trimmomatic_19_JOB_ID:$trimmomatic_20_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.ba0f872272bffc741dced4dd50383687.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.ba0f872272bffc741dced4dd50383687.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Paired Reads #	Surviving Paired Reads #	Surviving Paired Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/B11/B11_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/B11	B11_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/B12/B12_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/B12	B12_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/B1/B1_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/B1	B1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/B6/B6_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/B6	B6_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/B8/B8_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/B8	B8_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/C11/C11_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/C11	C11_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/C12/C12_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/C12	C12_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/C4/C4_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/C4	C4_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/C5/C5_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/C5	C5_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/C9/C9_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/C9	C9_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/D12/D12_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/D12	D12_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/D2/D2_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/D2	D2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/D3/D3_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/D3	D3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/D4/D4_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/D4	D4_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/D8/D8_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/D8	D8_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/E3/E3_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/E3	E3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/E4/E4_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/E4	E4_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/E6/E6_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/E6	E6_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/E8/E8_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/E8	E8_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/E9/E9_RS.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/E9	E9_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=32 \
  --variable read_type=Paired \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.ba0f872272bffc741dced4dd50383687.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: star
#-------------------------------------------------------------------------------
STEP=star
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: star_1_JOB_ID: star_align.1.B11_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.B11_RS
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/star/star_align.1.B11_RS.0c19bf00d16e2b3300afa1277e5244ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.B11_RS.0c19bf00d16e2b3300afa1277e5244ea.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/B11/B11_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/B11/B11_RS.trim.pair1.fastq.gz \
    trim/B11/B11_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/B11/B11_RS/ \
  --outSAMattrRGline ID:"B11_RS" 	PL:"ILLUMINA" 			SM:"B11" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.B11_RS.0c19bf00d16e2b3300afa1277e5244ea.mugqic.done
)
star_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_2_JOB_ID: star_align.1.B12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.B12_RS
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/star/star_align.1.B12_RS.9307ba66586d0fae100bbc76c5b98a92.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.B12_RS.9307ba66586d0fae100bbc76c5b98a92.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/B12/B12_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/B12/B12_RS.trim.pair1.fastq.gz \
    trim/B12/B12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/B12/B12_RS/ \
  --outSAMattrRGline ID:"B12_RS" 	PL:"ILLUMINA" 			SM:"B12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.B12_RS.9307ba66586d0fae100bbc76c5b98a92.mugqic.done
)
star_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_3_JOB_ID: star_align.1.B1_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.B1_RS
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/star/star_align.1.B1_RS.4cd1a01562e70bb225fff83dd60d0258.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.B1_RS.4cd1a01562e70bb225fff83dd60d0258.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/B1/B1_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/B1/B1_RS.trim.pair1.fastq.gz \
    trim/B1/B1_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/B1/B1_RS/ \
  --outSAMattrRGline ID:"B1_RS" 	PL:"ILLUMINA" 			SM:"B1" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.B1_RS.4cd1a01562e70bb225fff83dd60d0258.mugqic.done
)
star_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_4_JOB_ID: star_align.1.B6_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.B6_RS
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/star/star_align.1.B6_RS.c2b08e2f4c39d266b6e2cb95189c3b76.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.B6_RS.c2b08e2f4c39d266b6e2cb95189c3b76.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/B6/B6_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/B6/B6_RS.trim.pair1.fastq.gz \
    trim/B6/B6_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/B6/B6_RS/ \
  --outSAMattrRGline ID:"B6_RS" 	PL:"ILLUMINA" 			SM:"B6" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.B6_RS.c2b08e2f4c39d266b6e2cb95189c3b76.mugqic.done
)
star_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_5_JOB_ID: star_align.1.B8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.B8_RS
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/star/star_align.1.B8_RS.355d96ea4f8ae1b7752a881cd1c3809c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.B8_RS.355d96ea4f8ae1b7752a881cd1c3809c.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/B8/B8_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/B8/B8_RS.trim.pair1.fastq.gz \
    trim/B8/B8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/B8/B8_RS/ \
  --outSAMattrRGline ID:"B8_RS" 	PL:"ILLUMINA" 			SM:"B8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.B8_RS.355d96ea4f8ae1b7752a881cd1c3809c.mugqic.done
)
star_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_6_JOB_ID: star_align.1.C11_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.C11_RS
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/star/star_align.1.C11_RS.7d7a3b5c3b939c6c86a6929791d60ec2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.C11_RS.7d7a3b5c3b939c6c86a6929791d60ec2.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/C11/C11_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/C11/C11_RS.trim.pair1.fastq.gz \
    trim/C11/C11_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/C11/C11_RS/ \
  --outSAMattrRGline ID:"C11_RS" 	PL:"ILLUMINA" 			SM:"C11" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.C11_RS.7d7a3b5c3b939c6c86a6929791d60ec2.mugqic.done
)
star_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_7_JOB_ID: star_align.1.C12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.C12_RS
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID
JOB_DONE=job_output/star/star_align.1.C12_RS.fdd28cf5aed7da3c82a67715de340d9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.C12_RS.fdd28cf5aed7da3c82a67715de340d9c.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/C12/C12_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/C12/C12_RS.trim.pair1.fastq.gz \
    trim/C12/C12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/C12/C12_RS/ \
  --outSAMattrRGline ID:"C12_RS" 	PL:"ILLUMINA" 			SM:"C12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.C12_RS.fdd28cf5aed7da3c82a67715de340d9c.mugqic.done
)
star_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_8_JOB_ID: star_align.1.C4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.C4_RS
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID
JOB_DONE=job_output/star/star_align.1.C4_RS.9f97dc8e148e19380774c60c52fdbdd6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.C4_RS.9f97dc8e148e19380774c60c52fdbdd6.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/C4/C4_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/C4/C4_RS.trim.pair1.fastq.gz \
    trim/C4/C4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/C4/C4_RS/ \
  --outSAMattrRGline ID:"C4_RS" 	PL:"ILLUMINA" 			SM:"C4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.C4_RS.9f97dc8e148e19380774c60c52fdbdd6.mugqic.done
)
star_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_9_JOB_ID: star_align.1.C5_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.C5_RS
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID
JOB_DONE=job_output/star/star_align.1.C5_RS.731219f36e2e5074246931499f85a142.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.C5_RS.731219f36e2e5074246931499f85a142.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/C5/C5_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/C5/C5_RS.trim.pair1.fastq.gz \
    trim/C5/C5_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/C5/C5_RS/ \
  --outSAMattrRGline ID:"C5_RS" 	PL:"ILLUMINA" 			SM:"C5" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.C5_RS.731219f36e2e5074246931499f85a142.mugqic.done
)
star_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_10_JOB_ID: star_align.1.C9_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.C9_RS
JOB_DEPENDENCIES=$trimmomatic_10_JOB_ID
JOB_DONE=job_output/star/star_align.1.C9_RS.551114f8a230e743028741e3fc90ee54.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.C9_RS.551114f8a230e743028741e3fc90ee54.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/C9/C9_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/C9/C9_RS.trim.pair1.fastq.gz \
    trim/C9/C9_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/C9/C9_RS/ \
  --outSAMattrRGline ID:"C9_RS" 	PL:"ILLUMINA" 			SM:"C9" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.C9_RS.551114f8a230e743028741e3fc90ee54.mugqic.done
)
star_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_11_JOB_ID: star_align.1.D12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.D12_RS
JOB_DEPENDENCIES=$trimmomatic_11_JOB_ID
JOB_DONE=job_output/star/star_align.1.D12_RS.4a93cc5f573ea64d58be2a7f96348357.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.D12_RS.4a93cc5f573ea64d58be2a7f96348357.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/D12/D12_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/D12/D12_RS.trim.pair1.fastq.gz \
    trim/D12/D12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/D12/D12_RS/ \
  --outSAMattrRGline ID:"D12_RS" 	PL:"ILLUMINA" 			SM:"D12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.D12_RS.4a93cc5f573ea64d58be2a7f96348357.mugqic.done
)
star_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_12_JOB_ID: star_align.1.D2_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.D2_RS
JOB_DEPENDENCIES=$trimmomatic_12_JOB_ID
JOB_DONE=job_output/star/star_align.1.D2_RS.6a812303ecfd60c645ef4d2c030aac75.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.D2_RS.6a812303ecfd60c645ef4d2c030aac75.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/D2/D2_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/D2/D2_RS.trim.pair1.fastq.gz \
    trim/D2/D2_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/D2/D2_RS/ \
  --outSAMattrRGline ID:"D2_RS" 	PL:"ILLUMINA" 			SM:"D2" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.D2_RS.6a812303ecfd60c645ef4d2c030aac75.mugqic.done
)
star_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_13_JOB_ID: star_align.1.D3_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.D3_RS
JOB_DEPENDENCIES=$trimmomatic_13_JOB_ID
JOB_DONE=job_output/star/star_align.1.D3_RS.f48f6f3771fb75dbc92526c4d108518a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.D3_RS.f48f6f3771fb75dbc92526c4d108518a.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/D3/D3_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/D3/D3_RS.trim.pair1.fastq.gz \
    trim/D3/D3_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/D3/D3_RS/ \
  --outSAMattrRGline ID:"D3_RS" 	PL:"ILLUMINA" 			SM:"D3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.D3_RS.f48f6f3771fb75dbc92526c4d108518a.mugqic.done
)
star_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_14_JOB_ID: star_align.1.D4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.D4_RS
JOB_DEPENDENCIES=$trimmomatic_14_JOB_ID
JOB_DONE=job_output/star/star_align.1.D4_RS.5cf9b9ae494d2e69c16903d66d74576c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.D4_RS.5cf9b9ae494d2e69c16903d66d74576c.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/D4/D4_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/D4/D4_RS.trim.pair1.fastq.gz \
    trim/D4/D4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/D4/D4_RS/ \
  --outSAMattrRGline ID:"D4_RS" 	PL:"ILLUMINA" 			SM:"D4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.D4_RS.5cf9b9ae494d2e69c16903d66d74576c.mugqic.done
)
star_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_15_JOB_ID: star_align.1.D8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.D8_RS
JOB_DEPENDENCIES=$trimmomatic_15_JOB_ID
JOB_DONE=job_output/star/star_align.1.D8_RS.42eaa2eba130ecc9ae4d49226c0e5575.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.D8_RS.42eaa2eba130ecc9ae4d49226c0e5575.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/D8/D8_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/D8/D8_RS.trim.pair1.fastq.gz \
    trim/D8/D8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/D8/D8_RS/ \
  --outSAMattrRGline ID:"D8_RS" 	PL:"ILLUMINA" 			SM:"D8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.D8_RS.42eaa2eba130ecc9ae4d49226c0e5575.mugqic.done
)
star_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_16_JOB_ID: star_align.1.E3_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.E3_RS
JOB_DEPENDENCIES=$trimmomatic_16_JOB_ID
JOB_DONE=job_output/star/star_align.1.E3_RS.46494306ade7389335285a1e5ae5a633.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.E3_RS.46494306ade7389335285a1e5ae5a633.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/E3/E3_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/E3/E3_RS.trim.pair1.fastq.gz \
    trim/E3/E3_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/E3/E3_RS/ \
  --outSAMattrRGline ID:"E3_RS" 	PL:"ILLUMINA" 			SM:"E3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.E3_RS.46494306ade7389335285a1e5ae5a633.mugqic.done
)
star_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_17_JOB_ID: star_align.1.E4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.E4_RS
JOB_DEPENDENCIES=$trimmomatic_17_JOB_ID
JOB_DONE=job_output/star/star_align.1.E4_RS.b45d76984be2dee45bd4bb8debc8b333.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.E4_RS.b45d76984be2dee45bd4bb8debc8b333.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/E4/E4_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/E4/E4_RS.trim.pair1.fastq.gz \
    trim/E4/E4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/E4/E4_RS/ \
  --outSAMattrRGline ID:"E4_RS" 	PL:"ILLUMINA" 			SM:"E4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.E4_RS.b45d76984be2dee45bd4bb8debc8b333.mugqic.done
)
star_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_18_JOB_ID: star_align.1.E6_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.E6_RS
JOB_DEPENDENCIES=$trimmomatic_18_JOB_ID
JOB_DONE=job_output/star/star_align.1.E6_RS.e0a3fae883da23a9d55c458892c0fc53.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.E6_RS.e0a3fae883da23a9d55c458892c0fc53.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/E6/E6_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/E6/E6_RS.trim.pair1.fastq.gz \
    trim/E6/E6_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/E6/E6_RS/ \
  --outSAMattrRGline ID:"E6_RS" 	PL:"ILLUMINA" 			SM:"E6" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.E6_RS.e0a3fae883da23a9d55c458892c0fc53.mugqic.done
)
star_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_19_JOB_ID: star_align.1.E8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.E8_RS
JOB_DEPENDENCIES=$trimmomatic_19_JOB_ID
JOB_DONE=job_output/star/star_align.1.E8_RS.c4645b04584d4135f743390b7b4df9e5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.E8_RS.c4645b04584d4135f743390b7b4df9e5.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/E8/E8_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/E8/E8_RS.trim.pair1.fastq.gz \
    trim/E8/E8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/E8/E8_RS/ \
  --outSAMattrRGline ID:"E8_RS" 	PL:"ILLUMINA" 			SM:"E8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.E8_RS.c4645b04584d4135f743390b7b4df9e5.mugqic.done
)
star_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_20_JOB_ID: star_align.1.E9_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.E9_RS
JOB_DEPENDENCIES=$trimmomatic_20_JOB_ID
JOB_DONE=job_output/star/star_align.1.E9_RS.30f7d22b134a7f0f0a7fea142b5eb1e6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.E9_RS.30f7d22b134a7f0f0a7fea142b5eb1e6.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment_1stPass/E9/E9_RS && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/star_index/UCSC2012-02-07.sjdbOverhang99 \
  --readFilesIn \
    trim/E9/E9_RS.trim.pair1.fastq.gz \
    trim/E9/E9_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/E9/E9_RS/ \
  --outSAMattrRGline ID:"E9_RS" 	PL:"ILLUMINA" 			SM:"E9" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitIObufferSize 1000000000
star_align.1.E9_RS.30f7d22b134a7f0f0a7fea142b5eb1e6.mugqic.done
)
star_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_21_JOB_ID: star_index.AllSamples
#-------------------------------------------------------------------------------
JOB_NAME=star_index.AllSamples
JOB_DEPENDENCIES=$star_1_JOB_ID:$star_2_JOB_ID:$star_3_JOB_ID:$star_4_JOB_ID:$star_5_JOB_ID:$star_6_JOB_ID:$star_7_JOB_ID:$star_8_JOB_ID:$star_9_JOB_ID:$star_10_JOB_ID:$star_11_JOB_ID:$star_12_JOB_ID:$star_13_JOB_ID:$star_14_JOB_ID:$star_15_JOB_ID:$star_16_JOB_ID:$star_17_JOB_ID:$star_18_JOB_ID:$star_19_JOB_ID:$star_20_JOB_ID
JOB_DONE=job_output/star/star_index.AllSamples.4bf97c05f194094cb57c35ed0146107f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_index.AllSamples.4bf97c05f194094cb57c35ed0146107f.mugqic.done'
module load mugqic/star/2.5.0c mugqic/star/2.4.0f1 && \
cat \
  alignment_1stPass/B11/B11_RS/SJ.out.tab \
  alignment_1stPass/B12/B12_RS/SJ.out.tab \
  alignment_1stPass/B1/B1_RS/SJ.out.tab \
  alignment_1stPass/B6/B6_RS/SJ.out.tab \
  alignment_1stPass/B8/B8_RS/SJ.out.tab \
  alignment_1stPass/C11/C11_RS/SJ.out.tab \
  alignment_1stPass/C12/C12_RS/SJ.out.tab \
  alignment_1stPass/C4/C4_RS/SJ.out.tab \
  alignment_1stPass/C5/C5_RS/SJ.out.tab \
  alignment_1stPass/C9/C9_RS/SJ.out.tab \
  alignment_1stPass/D12/D12_RS/SJ.out.tab \
  alignment_1stPass/D2/D2_RS/SJ.out.tab \
  alignment_1stPass/D3/D3_RS/SJ.out.tab \
  alignment_1stPass/D4/D4_RS/SJ.out.tab \
  alignment_1stPass/D8/D8_RS/SJ.out.tab \
  alignment_1stPass/E3/E3_RS/SJ.out.tab \
  alignment_1stPass/E4/E4_RS/SJ.out.tab \
  alignment_1stPass/E6/E6_RS/SJ.out.tab \
  alignment_1stPass/E8/E8_RS/SJ.out.tab \
  alignment_1stPass/E9/E9_RS/SJ.out.tab | \
awk 'BEGIN {OFS="	"; strChar[0]="."; strChar[1]="+"; strChar[2]="-"} {if($5>0){print $1,$2,$3,strChar[$4]}}' | sort -k1,1h -k2,2n > alignment_1stPass/AllSamples.SJ.out.tab && \
mkdir -p reference.Merged && \
STAR --runMode genomeGenerate \
  --genomeDir reference.Merged \
  --genomeFastaFiles /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --runThreadN 12 \
  --limitGenomeGenerateRAM 50000000000 \
  --sjdbFileChrStartEnd alignment_1stPass/AllSamples.SJ.out.tab \
  --limitIObufferSize 1000000000 \
  --sjdbOverhang 99
star_index.AllSamples.4bf97c05f194094cb57c35ed0146107f.mugqic.done
)
star_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q lm -l nodes=1:ppn=12 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_22_JOB_ID: star_align.2.B11_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.B11_RS
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.B11_RS.eab1e4d3d5b268d3de35ebff0004255c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.B11_RS.eab1e4d3d5b268d3de35ebff0004255c.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/B11/B11_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/B11/B11_RS.trim.pair1.fastq.gz \
    trim/B11/B11_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/B11/B11_RS/ \
  --outSAMattrRGline ID:"B11_RS" 	PL:"ILLUMINA" 			SM:"B11" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f B11_RS/Aligned.sortedByCoord.out.bam alignment/B11/B11.sorted.bam
star_align.2.B11_RS.eab1e4d3d5b268d3de35ebff0004255c.mugqic.done
)
star_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_23_JOB_ID: star_align.2.B12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.B12_RS
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.B12_RS.d63ec650e28fc4eb21de4eb3ce1e531e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.B12_RS.d63ec650e28fc4eb21de4eb3ce1e531e.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/B12/B12_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/B12/B12_RS.trim.pair1.fastq.gz \
    trim/B12/B12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/B12/B12_RS/ \
  --outSAMattrRGline ID:"B12_RS" 	PL:"ILLUMINA" 			SM:"B12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f B12_RS/Aligned.sortedByCoord.out.bam alignment/B12/B12.sorted.bam
star_align.2.B12_RS.d63ec650e28fc4eb21de4eb3ce1e531e.mugqic.done
)
star_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_24_JOB_ID: star_align.2.B1_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.B1_RS
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.B1_RS.e5dcd2685436bfa360c43d3cd05479ca.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.B1_RS.e5dcd2685436bfa360c43d3cd05479ca.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/B1/B1_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/B1/B1_RS.trim.pair1.fastq.gz \
    trim/B1/B1_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/B1/B1_RS/ \
  --outSAMattrRGline ID:"B1_RS" 	PL:"ILLUMINA" 			SM:"B1" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f B1_RS/Aligned.sortedByCoord.out.bam alignment/B1/B1.sorted.bam
star_align.2.B1_RS.e5dcd2685436bfa360c43d3cd05479ca.mugqic.done
)
star_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_25_JOB_ID: star_align.2.B6_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.B6_RS
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.B6_RS.f2f8eff6056c0210b43a4dcb8667e946.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.B6_RS.f2f8eff6056c0210b43a4dcb8667e946.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/B6/B6_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/B6/B6_RS.trim.pair1.fastq.gz \
    trim/B6/B6_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/B6/B6_RS/ \
  --outSAMattrRGline ID:"B6_RS" 	PL:"ILLUMINA" 			SM:"B6" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f B6_RS/Aligned.sortedByCoord.out.bam alignment/B6/B6.sorted.bam
star_align.2.B6_RS.f2f8eff6056c0210b43a4dcb8667e946.mugqic.done
)
star_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_26_JOB_ID: star_align.2.B8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.B8_RS
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.B8_RS.203c002c62fc5fc598d06c6a025840e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.B8_RS.203c002c62fc5fc598d06c6a025840e8.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/B8/B8_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/B8/B8_RS.trim.pair1.fastq.gz \
    trim/B8/B8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/B8/B8_RS/ \
  --outSAMattrRGline ID:"B8_RS" 	PL:"ILLUMINA" 			SM:"B8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f B8_RS/Aligned.sortedByCoord.out.bam alignment/B8/B8.sorted.bam
star_align.2.B8_RS.203c002c62fc5fc598d06c6a025840e8.mugqic.done
)
star_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_27_JOB_ID: star_align.2.C11_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.C11_RS
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.C11_RS.917afe516cf90ecfb9bb54e6bba449f8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.C11_RS.917afe516cf90ecfb9bb54e6bba449f8.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/C11/C11_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/C11/C11_RS.trim.pair1.fastq.gz \
    trim/C11/C11_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/C11/C11_RS/ \
  --outSAMattrRGline ID:"C11_RS" 	PL:"ILLUMINA" 			SM:"C11" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f C11_RS/Aligned.sortedByCoord.out.bam alignment/C11/C11.sorted.bam
star_align.2.C11_RS.917afe516cf90ecfb9bb54e6bba449f8.mugqic.done
)
star_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_28_JOB_ID: star_align.2.C12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.C12_RS
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.C12_RS.d629d3622b5665dd1c111584fd567db2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.C12_RS.d629d3622b5665dd1c111584fd567db2.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/C12/C12_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/C12/C12_RS.trim.pair1.fastq.gz \
    trim/C12/C12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/C12/C12_RS/ \
  --outSAMattrRGline ID:"C12_RS" 	PL:"ILLUMINA" 			SM:"C12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f C12_RS/Aligned.sortedByCoord.out.bam alignment/C12/C12.sorted.bam
star_align.2.C12_RS.d629d3622b5665dd1c111584fd567db2.mugqic.done
)
star_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_29_JOB_ID: star_align.2.C4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.C4_RS
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.C4_RS.06bafc2be1e0858cc7dd0277eeba0642.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.C4_RS.06bafc2be1e0858cc7dd0277eeba0642.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/C4/C4_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/C4/C4_RS.trim.pair1.fastq.gz \
    trim/C4/C4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/C4/C4_RS/ \
  --outSAMattrRGline ID:"C4_RS" 	PL:"ILLUMINA" 			SM:"C4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f C4_RS/Aligned.sortedByCoord.out.bam alignment/C4/C4.sorted.bam
star_align.2.C4_RS.06bafc2be1e0858cc7dd0277eeba0642.mugqic.done
)
star_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_30_JOB_ID: star_align.2.C5_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.C5_RS
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.C5_RS.59c4e1c0ba75ee87e9bc35cab4c3b34a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.C5_RS.59c4e1c0ba75ee87e9bc35cab4c3b34a.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/C5/C5_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/C5/C5_RS.trim.pair1.fastq.gz \
    trim/C5/C5_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/C5/C5_RS/ \
  --outSAMattrRGline ID:"C5_RS" 	PL:"ILLUMINA" 			SM:"C5" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f C5_RS/Aligned.sortedByCoord.out.bam alignment/C5/C5.sorted.bam
star_align.2.C5_RS.59c4e1c0ba75ee87e9bc35cab4c3b34a.mugqic.done
)
star_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_31_JOB_ID: star_align.2.C9_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.C9_RS
JOB_DEPENDENCIES=$trimmomatic_10_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.C9_RS.693e86b4cbb750e59218e295e1063133.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.C9_RS.693e86b4cbb750e59218e295e1063133.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/C9/C9_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/C9/C9_RS.trim.pair1.fastq.gz \
    trim/C9/C9_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/C9/C9_RS/ \
  --outSAMattrRGline ID:"C9_RS" 	PL:"ILLUMINA" 			SM:"C9" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f C9_RS/Aligned.sortedByCoord.out.bam alignment/C9/C9.sorted.bam
star_align.2.C9_RS.693e86b4cbb750e59218e295e1063133.mugqic.done
)
star_31_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_32_JOB_ID: star_align.2.D12_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.D12_RS
JOB_DEPENDENCIES=$trimmomatic_11_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.D12_RS.3ef9315c1ff05a49fb4a03f05028aa36.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.D12_RS.3ef9315c1ff05a49fb4a03f05028aa36.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/D12/D12_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/D12/D12_RS.trim.pair1.fastq.gz \
    trim/D12/D12_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/D12/D12_RS/ \
  --outSAMattrRGline ID:"D12_RS" 	PL:"ILLUMINA" 			SM:"D12" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f D12_RS/Aligned.sortedByCoord.out.bam alignment/D12/D12.sorted.bam
star_align.2.D12_RS.3ef9315c1ff05a49fb4a03f05028aa36.mugqic.done
)
star_32_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_33_JOB_ID: star_align.2.D2_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.D2_RS
JOB_DEPENDENCIES=$trimmomatic_12_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.D2_RS.4af44fa1ae7f7855e78081a678b7bb83.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.D2_RS.4af44fa1ae7f7855e78081a678b7bb83.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/D2/D2_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/D2/D2_RS.trim.pair1.fastq.gz \
    trim/D2/D2_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/D2/D2_RS/ \
  --outSAMattrRGline ID:"D2_RS" 	PL:"ILLUMINA" 			SM:"D2" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f D2_RS/Aligned.sortedByCoord.out.bam alignment/D2/D2.sorted.bam
star_align.2.D2_RS.4af44fa1ae7f7855e78081a678b7bb83.mugqic.done
)
star_33_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_34_JOB_ID: star_align.2.D3_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.D3_RS
JOB_DEPENDENCIES=$trimmomatic_13_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.D3_RS.52fef9ba74df1648b84459543bd81129.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.D3_RS.52fef9ba74df1648b84459543bd81129.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/D3/D3_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/D3/D3_RS.trim.pair1.fastq.gz \
    trim/D3/D3_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/D3/D3_RS/ \
  --outSAMattrRGline ID:"D3_RS" 	PL:"ILLUMINA" 			SM:"D3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f D3_RS/Aligned.sortedByCoord.out.bam alignment/D3/D3.sorted.bam
star_align.2.D3_RS.52fef9ba74df1648b84459543bd81129.mugqic.done
)
star_34_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_35_JOB_ID: star_align.2.D4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.D4_RS
JOB_DEPENDENCIES=$trimmomatic_14_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.D4_RS.88506ca5f22701132350f687ecda25bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.D4_RS.88506ca5f22701132350f687ecda25bc.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/D4/D4_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/D4/D4_RS.trim.pair1.fastq.gz \
    trim/D4/D4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/D4/D4_RS/ \
  --outSAMattrRGline ID:"D4_RS" 	PL:"ILLUMINA" 			SM:"D4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f D4_RS/Aligned.sortedByCoord.out.bam alignment/D4/D4.sorted.bam
star_align.2.D4_RS.88506ca5f22701132350f687ecda25bc.mugqic.done
)
star_35_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_36_JOB_ID: star_align.2.D8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.D8_RS
JOB_DEPENDENCIES=$trimmomatic_15_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.D8_RS.dcdd8b68f0731b9d4d09629bf17e2dac.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.D8_RS.dcdd8b68f0731b9d4d09629bf17e2dac.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/D8/D8_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/D8/D8_RS.trim.pair1.fastq.gz \
    trim/D8/D8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/D8/D8_RS/ \
  --outSAMattrRGline ID:"D8_RS" 	PL:"ILLUMINA" 			SM:"D8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f D8_RS/Aligned.sortedByCoord.out.bam alignment/D8/D8.sorted.bam
star_align.2.D8_RS.dcdd8b68f0731b9d4d09629bf17e2dac.mugqic.done
)
star_36_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_37_JOB_ID: star_align.2.E3_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.E3_RS
JOB_DEPENDENCIES=$trimmomatic_16_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.E3_RS.ed83276b3b566fb6ea224b0cd24bc60b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.E3_RS.ed83276b3b566fb6ea224b0cd24bc60b.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/E3/E3_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/E3/E3_RS.trim.pair1.fastq.gz \
    trim/E3/E3_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/E3/E3_RS/ \
  --outSAMattrRGline ID:"E3_RS" 	PL:"ILLUMINA" 			SM:"E3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f E3_RS/Aligned.sortedByCoord.out.bam alignment/E3/E3.sorted.bam
star_align.2.E3_RS.ed83276b3b566fb6ea224b0cd24bc60b.mugqic.done
)
star_37_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_38_JOB_ID: star_align.2.E4_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.E4_RS
JOB_DEPENDENCIES=$trimmomatic_17_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.E4_RS.063dbe19f9d1cfea7d7a631c645c079a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.E4_RS.063dbe19f9d1cfea7d7a631c645c079a.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/E4/E4_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/E4/E4_RS.trim.pair1.fastq.gz \
    trim/E4/E4_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/E4/E4_RS/ \
  --outSAMattrRGline ID:"E4_RS" 	PL:"ILLUMINA" 			SM:"E4" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f E4_RS/Aligned.sortedByCoord.out.bam alignment/E4/E4.sorted.bam
star_align.2.E4_RS.063dbe19f9d1cfea7d7a631c645c079a.mugqic.done
)
star_38_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_39_JOB_ID: star_align.2.E6_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.E6_RS
JOB_DEPENDENCIES=$trimmomatic_18_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.E6_RS.9feb13287ad9aae92a0073b18cedeca2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.E6_RS.9feb13287ad9aae92a0073b18cedeca2.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/E6/E6_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/E6/E6_RS.trim.pair1.fastq.gz \
    trim/E6/E6_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/E6/E6_RS/ \
  --outSAMattrRGline ID:"E6_RS" 	PL:"ILLUMINA" 			SM:"E6" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f E6_RS/Aligned.sortedByCoord.out.bam alignment/E6/E6.sorted.bam
star_align.2.E6_RS.9feb13287ad9aae92a0073b18cedeca2.mugqic.done
)
star_39_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_40_JOB_ID: star_align.2.E8_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.E8_RS
JOB_DEPENDENCIES=$trimmomatic_19_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.E8_RS.dab576889b9abbbe3ac0ac0f1ac679b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.E8_RS.dab576889b9abbbe3ac0ac0f1ac679b4.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/E8/E8_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/E8/E8_RS.trim.pair1.fastq.gz \
    trim/E8/E8_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/E8/E8_RS/ \
  --outSAMattrRGline ID:"E8_RS" 	PL:"ILLUMINA" 			SM:"E8" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f E8_RS/Aligned.sortedByCoord.out.bam alignment/E8/E8.sorted.bam
star_align.2.E8_RS.dab576889b9abbbe3ac0ac0f1ac679b4.mugqic.done
)
star_40_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_41_JOB_ID: star_align.2.E9_RS
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.E9_RS
JOB_DEPENDENCIES=$trimmomatic_20_JOB_ID:$star_21_JOB_ID
JOB_DONE=job_output/star/star_align.2.E9_RS.202d583e45bc7b60d4a9626288e74ed9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.E9_RS.202d583e45bc7b60d4a9626288e74ed9.mugqic.done'
module load mugqic/star/2.4.0f1 && \
mkdir -p alignment/E9/E9_RS && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/E9/E9_RS.trim.pair1.fastq.gz \
    trim/E9/E9_RS.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/E9/E9_RS/ \
  --outSAMattrRGline ID:"E9_RS" 	PL:"ILLUMINA" 			SM:"E9" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 70000000000 \
  --limitBAMsortRAM 70000000000 \
  --limitIObufferSize 1000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f E9_RS/Aligned.sortedByCoord.out.bam alignment/E9/E9.sorted.bam
star_align.2.E9_RS.202d583e45bc7b60d4a9626288e74ed9.mugqic.done
)
star_41_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=16 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_42_JOB_ID: star_report
#-------------------------------------------------------------------------------
JOB_NAME=star_report
JOB_DEPENDENCIES=$star_22_JOB_ID:$star_23_JOB_ID:$star_24_JOB_ID:$star_25_JOB_ID:$star_26_JOB_ID:$star_27_JOB_ID:$star_28_JOB_ID:$star_29_JOB_ID:$star_30_JOB_ID:$star_31_JOB_ID:$star_32_JOB_ID:$star_33_JOB_ID:$star_34_JOB_ID:$star_35_JOB_ID:$star_36_JOB_ID:$star_37_JOB_ID:$star_38_JOB_ID:$star_39_JOB_ID:$star_40_JOB_ID:$star_41_JOB_ID
JOB_DONE=job_output/star/star_report.e4461a8e56f30ba0dac913c1ce71f2ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_report.e4461a8e56f30ba0dac913c1ce71f2ed.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.star.md \
  --variable scientific_name="Mus_musculus" \
  --variable assembly="mm10" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.star.md \
  > report/RnaSeq.star.md
star_report.e4461a8e56f30ba0dac913c1ce71f2ed.mugqic.done
)
star_42_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$star_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_sort_sam
#-------------------------------------------------------------------------------
STEP=picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_1_JOB_ID: picard_sort_sam.B11
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.B11
JOB_DEPENDENCIES=$star_22_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.B11.87b2b945aa361a119826b3006b90edf2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.B11.87b2b945aa361a119826b3006b90edf2.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B11/B11.sorted.bam \
  OUTPUT=alignment/B11/B11.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.B11.87b2b945aa361a119826b3006b90edf2.mugqic.done
)
picard_sort_sam_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_2_JOB_ID: picard_sort_sam.B12
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.B12
JOB_DEPENDENCIES=$star_23_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.B12.3b8eab29486534f72ba930aed54cccd8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.B12.3b8eab29486534f72ba930aed54cccd8.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B12/B12.sorted.bam \
  OUTPUT=alignment/B12/B12.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.B12.3b8eab29486534f72ba930aed54cccd8.mugqic.done
)
picard_sort_sam_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_3_JOB_ID: picard_sort_sam.B1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.B1
JOB_DEPENDENCIES=$star_24_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.B1.bd6979c704c5fb016dc9ec0964b94af5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.B1.bd6979c704c5fb016dc9ec0964b94af5.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B1/B1.sorted.bam \
  OUTPUT=alignment/B1/B1.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.B1.bd6979c704c5fb016dc9ec0964b94af5.mugqic.done
)
picard_sort_sam_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_4_JOB_ID: picard_sort_sam.B6
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.B6
JOB_DEPENDENCIES=$star_25_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.B6.267630df3c2554af6935af1be81a56df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.B6.267630df3c2554af6935af1be81a56df.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B6/B6.sorted.bam \
  OUTPUT=alignment/B6/B6.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.B6.267630df3c2554af6935af1be81a56df.mugqic.done
)
picard_sort_sam_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_5_JOB_ID: picard_sort_sam.B8
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.B8
JOB_DEPENDENCIES=$star_26_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.B8.fb31b173588261d477e9efbc8a1bcfd0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.B8.fb31b173588261d477e9efbc8a1bcfd0.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B8/B8.sorted.bam \
  OUTPUT=alignment/B8/B8.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.B8.fb31b173588261d477e9efbc8a1bcfd0.mugqic.done
)
picard_sort_sam_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_6_JOB_ID: picard_sort_sam.C11
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.C11
JOB_DEPENDENCIES=$star_27_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.C11.de9c4035d967a64ea629e6e995edd4e7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.C11.de9c4035d967a64ea629e6e995edd4e7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C11/C11.sorted.bam \
  OUTPUT=alignment/C11/C11.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.C11.de9c4035d967a64ea629e6e995edd4e7.mugqic.done
)
picard_sort_sam_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_7_JOB_ID: picard_sort_sam.C12
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.C12
JOB_DEPENDENCIES=$star_28_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.C12.28f2e8d2a60a69f2bc1517747cfae55d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.C12.28f2e8d2a60a69f2bc1517747cfae55d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C12/C12.sorted.bam \
  OUTPUT=alignment/C12/C12.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.C12.28f2e8d2a60a69f2bc1517747cfae55d.mugqic.done
)
picard_sort_sam_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_8_JOB_ID: picard_sort_sam.C4
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.C4
JOB_DEPENDENCIES=$star_29_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.C4.2e57e7aeca0621e6e405f569a9dc2478.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.C4.2e57e7aeca0621e6e405f569a9dc2478.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C4/C4.sorted.bam \
  OUTPUT=alignment/C4/C4.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.C4.2e57e7aeca0621e6e405f569a9dc2478.mugqic.done
)
picard_sort_sam_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_9_JOB_ID: picard_sort_sam.C5
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.C5
JOB_DEPENDENCIES=$star_30_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.C5.39552d2a5f0aa8f4d056256b8858f31d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.C5.39552d2a5f0aa8f4d056256b8858f31d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C5/C5.sorted.bam \
  OUTPUT=alignment/C5/C5.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.C5.39552d2a5f0aa8f4d056256b8858f31d.mugqic.done
)
picard_sort_sam_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_10_JOB_ID: picard_sort_sam.C9
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.C9
JOB_DEPENDENCIES=$star_31_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.C9.75e2e1beaf46e8e74b3b8eb8f658eb3d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.C9.75e2e1beaf46e8e74b3b8eb8f658eb3d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C9/C9.sorted.bam \
  OUTPUT=alignment/C9/C9.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.C9.75e2e1beaf46e8e74b3b8eb8f658eb3d.mugqic.done
)
picard_sort_sam_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_11_JOB_ID: picard_sort_sam.D12
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.D12
JOB_DEPENDENCIES=$star_32_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.D12.dd62b2460253584614d5d26ee9061ee4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.D12.dd62b2460253584614d5d26ee9061ee4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D12/D12.sorted.bam \
  OUTPUT=alignment/D12/D12.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.D12.dd62b2460253584614d5d26ee9061ee4.mugqic.done
)
picard_sort_sam_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_12_JOB_ID: picard_sort_sam.D2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.D2
JOB_DEPENDENCIES=$star_33_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.D2.4c3cd8def3aeb6d3c80b600947a59b9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.D2.4c3cd8def3aeb6d3c80b600947a59b9c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D2/D2.sorted.bam \
  OUTPUT=alignment/D2/D2.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.D2.4c3cd8def3aeb6d3c80b600947a59b9c.mugqic.done
)
picard_sort_sam_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_13_JOB_ID: picard_sort_sam.D3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.D3
JOB_DEPENDENCIES=$star_34_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.D3.82d3466fac569613383ffbee098f9d45.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.D3.82d3466fac569613383ffbee098f9d45.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D3/D3.sorted.bam \
  OUTPUT=alignment/D3/D3.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.D3.82d3466fac569613383ffbee098f9d45.mugqic.done
)
picard_sort_sam_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_14_JOB_ID: picard_sort_sam.D4
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.D4
JOB_DEPENDENCIES=$star_35_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.D4.67bf97aaf2ddb9ae2a256c7b78fc7787.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.D4.67bf97aaf2ddb9ae2a256c7b78fc7787.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D4/D4.sorted.bam \
  OUTPUT=alignment/D4/D4.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.D4.67bf97aaf2ddb9ae2a256c7b78fc7787.mugqic.done
)
picard_sort_sam_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_15_JOB_ID: picard_sort_sam.D8
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.D8
JOB_DEPENDENCIES=$star_36_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.D8.f4ecddf26149bf0e3fdca3c8e366a505.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.D8.f4ecddf26149bf0e3fdca3c8e366a505.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D8/D8.sorted.bam \
  OUTPUT=alignment/D8/D8.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.D8.f4ecddf26149bf0e3fdca3c8e366a505.mugqic.done
)
picard_sort_sam_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_16_JOB_ID: picard_sort_sam.E3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.E3
JOB_DEPENDENCIES=$star_37_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.E3.0e957292f488b6ecfadb3e9a7a5c5175.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.E3.0e957292f488b6ecfadb3e9a7a5c5175.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E3/E3.sorted.bam \
  OUTPUT=alignment/E3/E3.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.E3.0e957292f488b6ecfadb3e9a7a5c5175.mugqic.done
)
picard_sort_sam_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_17_JOB_ID: picard_sort_sam.E4
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.E4
JOB_DEPENDENCIES=$star_38_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.E4.709f973892c33d1eb539cf32ff16c8d9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.E4.709f973892c33d1eb539cf32ff16c8d9.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E4/E4.sorted.bam \
  OUTPUT=alignment/E4/E4.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.E4.709f973892c33d1eb539cf32ff16c8d9.mugqic.done
)
picard_sort_sam_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_18_JOB_ID: picard_sort_sam.E6
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.E6
JOB_DEPENDENCIES=$star_39_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.E6.64f880de8cad292dc76ed23af831fb2e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.E6.64f880de8cad292dc76ed23af831fb2e.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E6/E6.sorted.bam \
  OUTPUT=alignment/E6/E6.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.E6.64f880de8cad292dc76ed23af831fb2e.mugqic.done
)
picard_sort_sam_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_19_JOB_ID: picard_sort_sam.E8
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.E8
JOB_DEPENDENCIES=$star_40_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.E8.43fc6aa4778b2c798fd5e9b04b88a818.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.E8.43fc6aa4778b2c798fd5e9b04b88a818.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E8/E8.sorted.bam \
  OUTPUT=alignment/E8/E8.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.E8.43fc6aa4778b2c798fd5e9b04b88a818.mugqic.done
)
picard_sort_sam_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_20_JOB_ID: picard_sort_sam.E9
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.E9
JOB_DEPENDENCIES=$star_41_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.E9.7c26046757b45bec156c4c4641d23ce2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.E9.7c26046757b45bec156c4c4641d23ce2.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E9/E9.sorted.bam \
  OUTPUT=alignment/E9/E9.QueryNameSorted.bam \
  SORT_ORDER=queryname \
  MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.E9.7c26046757b45bec156c4c4641d23ce2.mugqic.done
)
picard_sort_sam_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_sort_sam_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.B11
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.B11
JOB_DEPENDENCIES=$star_22_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.B11.ba186a1fa31cca2aafd8c7d699cdcb97.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.B11.ba186a1fa31cca2aafd8c7d699cdcb97.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B11/B11.sorted.bam \
  OUTPUT=alignment/B11/B11.sorted.mdup.bam \
  METRICS_FILE=alignment/B11/B11.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.B11.ba186a1fa31cca2aafd8c7d699cdcb97.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.B12
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.B12
JOB_DEPENDENCIES=$star_23_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.B12.eb24fe0442d47c9ddf87ff5598d0cfa2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.B12.eb24fe0442d47c9ddf87ff5598d0cfa2.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B12/B12.sorted.bam \
  OUTPUT=alignment/B12/B12.sorted.mdup.bam \
  METRICS_FILE=alignment/B12/B12.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.B12.eb24fe0442d47c9ddf87ff5598d0cfa2.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.B1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.B1
JOB_DEPENDENCIES=$star_24_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.B1.b36b2ae1bfec598df7a48fef536a2140.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.B1.b36b2ae1bfec598df7a48fef536a2140.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B1/B1.sorted.bam \
  OUTPUT=alignment/B1/B1.sorted.mdup.bam \
  METRICS_FILE=alignment/B1/B1.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.B1.b36b2ae1bfec598df7a48fef536a2140.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.B6
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.B6
JOB_DEPENDENCIES=$star_25_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.B6.3052f2730c5bf363769e3026bb0d4219.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.B6.3052f2730c5bf363769e3026bb0d4219.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B6/B6.sorted.bam \
  OUTPUT=alignment/B6/B6.sorted.mdup.bam \
  METRICS_FILE=alignment/B6/B6.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.B6.3052f2730c5bf363769e3026bb0d4219.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.B8
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.B8
JOB_DEPENDENCIES=$star_26_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.B8.3daf8a0d6b4d7730d33635ec3d79b92e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.B8.3daf8a0d6b4d7730d33635ec3d79b92e.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B8/B8.sorted.bam \
  OUTPUT=alignment/B8/B8.sorted.mdup.bam \
  METRICS_FILE=alignment/B8/B8.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.B8.3daf8a0d6b4d7730d33635ec3d79b92e.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.C11
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.C11
JOB_DEPENDENCIES=$star_27_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.C11.e7f62d26a1af8137942274f401f8f3b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.C11.e7f62d26a1af8137942274f401f8f3b4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C11/C11.sorted.bam \
  OUTPUT=alignment/C11/C11.sorted.mdup.bam \
  METRICS_FILE=alignment/C11/C11.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.C11.e7f62d26a1af8137942274f401f8f3b4.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.C12
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.C12
JOB_DEPENDENCIES=$star_28_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.C12.06327325f4c7e01742b347264737b434.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.C12.06327325f4c7e01742b347264737b434.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C12/C12.sorted.bam \
  OUTPUT=alignment/C12/C12.sorted.mdup.bam \
  METRICS_FILE=alignment/C12/C12.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.C12.06327325f4c7e01742b347264737b434.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.C4
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.C4
JOB_DEPENDENCIES=$star_29_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.C4.0ccc37885837fcbfb80006db245ea62b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.C4.0ccc37885837fcbfb80006db245ea62b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C4/C4.sorted.bam \
  OUTPUT=alignment/C4/C4.sorted.mdup.bam \
  METRICS_FILE=alignment/C4/C4.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.C4.0ccc37885837fcbfb80006db245ea62b.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates.C5
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.C5
JOB_DEPENDENCIES=$star_30_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.C5.b0d681cda74fb6874cc2358aee835394.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.C5.b0d681cda74fb6874cc2358aee835394.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C5/C5.sorted.bam \
  OUTPUT=alignment/C5/C5.sorted.mdup.bam \
  METRICS_FILE=alignment/C5/C5.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.C5.b0d681cda74fb6874cc2358aee835394.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_10_JOB_ID: picard_mark_duplicates.C9
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.C9
JOB_DEPENDENCIES=$star_31_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.C9.d830dd7e683e94a06bd5b57556af5db4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.C9.d830dd7e683e94a06bd5b57556af5db4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C9/C9.sorted.bam \
  OUTPUT=alignment/C9/C9.sorted.mdup.bam \
  METRICS_FILE=alignment/C9/C9.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.C9.d830dd7e683e94a06bd5b57556af5db4.mugqic.done
)
picard_mark_duplicates_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_11_JOB_ID: picard_mark_duplicates.D12
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.D12
JOB_DEPENDENCIES=$star_32_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.D12.739c15b6c42299ba8d17b42999c696e2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.D12.739c15b6c42299ba8d17b42999c696e2.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D12/D12.sorted.bam \
  OUTPUT=alignment/D12/D12.sorted.mdup.bam \
  METRICS_FILE=alignment/D12/D12.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.D12.739c15b6c42299ba8d17b42999c696e2.mugqic.done
)
picard_mark_duplicates_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_12_JOB_ID: picard_mark_duplicates.D2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.D2
JOB_DEPENDENCIES=$star_33_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.D2.948cf4213b7b71fb8e5a770fb01ae7c7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.D2.948cf4213b7b71fb8e5a770fb01ae7c7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D2/D2.sorted.bam \
  OUTPUT=alignment/D2/D2.sorted.mdup.bam \
  METRICS_FILE=alignment/D2/D2.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.D2.948cf4213b7b71fb8e5a770fb01ae7c7.mugqic.done
)
picard_mark_duplicates_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_13_JOB_ID: picard_mark_duplicates.D3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.D3
JOB_DEPENDENCIES=$star_34_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.D3.c022fa4265e5dc2bec355b959d901231.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.D3.c022fa4265e5dc2bec355b959d901231.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D3/D3.sorted.bam \
  OUTPUT=alignment/D3/D3.sorted.mdup.bam \
  METRICS_FILE=alignment/D3/D3.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.D3.c022fa4265e5dc2bec355b959d901231.mugqic.done
)
picard_mark_duplicates_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_14_JOB_ID: picard_mark_duplicates.D4
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.D4
JOB_DEPENDENCIES=$star_35_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.D4.4f0f6ff1f9988e6e0a39ab15ab9e963c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.D4.4f0f6ff1f9988e6e0a39ab15ab9e963c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D4/D4.sorted.bam \
  OUTPUT=alignment/D4/D4.sorted.mdup.bam \
  METRICS_FILE=alignment/D4/D4.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.D4.4f0f6ff1f9988e6e0a39ab15ab9e963c.mugqic.done
)
picard_mark_duplicates_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_15_JOB_ID: picard_mark_duplicates.D8
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.D8
JOB_DEPENDENCIES=$star_36_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.D8.d2ed5e19f8616910a4dbc1f890be2fa4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.D8.d2ed5e19f8616910a4dbc1f890be2fa4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D8/D8.sorted.bam \
  OUTPUT=alignment/D8/D8.sorted.mdup.bam \
  METRICS_FILE=alignment/D8/D8.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.D8.d2ed5e19f8616910a4dbc1f890be2fa4.mugqic.done
)
picard_mark_duplicates_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_16_JOB_ID: picard_mark_duplicates.E3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.E3
JOB_DEPENDENCIES=$star_37_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.E3.9044c4096ea39fb770e246d3fb95e569.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.E3.9044c4096ea39fb770e246d3fb95e569.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E3/E3.sorted.bam \
  OUTPUT=alignment/E3/E3.sorted.mdup.bam \
  METRICS_FILE=alignment/E3/E3.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.E3.9044c4096ea39fb770e246d3fb95e569.mugqic.done
)
picard_mark_duplicates_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_17_JOB_ID: picard_mark_duplicates.E4
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.E4
JOB_DEPENDENCIES=$star_38_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.E4.17f69a4f40e3f12932360d600f75af93.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.E4.17f69a4f40e3f12932360d600f75af93.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E4/E4.sorted.bam \
  OUTPUT=alignment/E4/E4.sorted.mdup.bam \
  METRICS_FILE=alignment/E4/E4.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.E4.17f69a4f40e3f12932360d600f75af93.mugqic.done
)
picard_mark_duplicates_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_18_JOB_ID: picard_mark_duplicates.E6
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.E6
JOB_DEPENDENCIES=$star_39_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.E6.5d119ddb1fdb30b097bae8ac32594a10.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.E6.5d119ddb1fdb30b097bae8ac32594a10.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E6/E6.sorted.bam \
  OUTPUT=alignment/E6/E6.sorted.mdup.bam \
  METRICS_FILE=alignment/E6/E6.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.E6.5d119ddb1fdb30b097bae8ac32594a10.mugqic.done
)
picard_mark_duplicates_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_19_JOB_ID: picard_mark_duplicates.E8
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.E8
JOB_DEPENDENCIES=$star_40_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.E8.0b1d8b2fce3a6db334087adc749b6be7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.E8.0b1d8b2fce3a6db334087adc749b6be7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E8/E8.sorted.bam \
  OUTPUT=alignment/E8/E8.sorted.mdup.bam \
  METRICS_FILE=alignment/E8/E8.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.E8.0b1d8b2fce3a6db334087adc749b6be7.mugqic.done
)
picard_mark_duplicates_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_20_JOB_ID: picard_mark_duplicates.E9
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.E9
JOB_DEPENDENCIES=$star_41_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.E9.d10be3bcb62889d29699a91e9382ecf0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.E9.d10be3bcb62889d29699a91e9382ecf0.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx14G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E9/E9.sorted.bam \
  OUTPUT=alignment/E9/E9.sorted.mdup.bam \
  METRICS_FILE=alignment/E9/E9.sorted.mdup.metrics \
  MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.E9.d10be3bcb62889d29699a91e9382ecf0.mugqic.done
)
picard_mark_duplicates_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=6 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_rna_metrics
#-------------------------------------------------------------------------------
STEP=picard_rna_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_1_JOB_ID: picard_rna_metrics.B11
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.B11
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.B11.4fdae028089a7444957a889800b49d04.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.B11.4fdae028089a7444957a889800b49d04.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/B11 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/B11/B11.sorted.mdup.bam \
  OUTPUT=metrics/B11/B11 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B11/B11.sorted.mdup.bam \
  OUTPUT=metrics/B11/B11.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.B11.4fdae028089a7444957a889800b49d04.mugqic.done
)
picard_rna_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_2_JOB_ID: picard_rna_metrics.B12
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.B12
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.B12.ed55026dbd8cb30c72028562f4972f4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.B12.ed55026dbd8cb30c72028562f4972f4f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/B12 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/B12/B12.sorted.mdup.bam \
  OUTPUT=metrics/B12/B12 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B12/B12.sorted.mdup.bam \
  OUTPUT=metrics/B12/B12.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.B12.ed55026dbd8cb30c72028562f4972f4f.mugqic.done
)
picard_rna_metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_3_JOB_ID: picard_rna_metrics.B1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.B1
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.B1.3287da6d3ff8bf6069e73249b897eff3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.B1.3287da6d3ff8bf6069e73249b897eff3.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/B1 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/B1/B1.sorted.mdup.bam \
  OUTPUT=metrics/B1/B1 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B1/B1.sorted.mdup.bam \
  OUTPUT=metrics/B1/B1.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.B1.3287da6d3ff8bf6069e73249b897eff3.mugqic.done
)
picard_rna_metrics_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_4_JOB_ID: picard_rna_metrics.B6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.B6
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.B6.a463a6bcbfd9b8109483246f8ea92253.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.B6.a463a6bcbfd9b8109483246f8ea92253.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/B6 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/B6/B6.sorted.mdup.bam \
  OUTPUT=metrics/B6/B6 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B6/B6.sorted.mdup.bam \
  OUTPUT=metrics/B6/B6.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.B6.a463a6bcbfd9b8109483246f8ea92253.mugqic.done
)
picard_rna_metrics_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_5_JOB_ID: picard_rna_metrics.B8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.B8
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.B8.4c0ed8153fdb2f4a3262e84999293e3d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.B8.4c0ed8153fdb2f4a3262e84999293e3d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/B8 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/B8/B8.sorted.mdup.bam \
  OUTPUT=metrics/B8/B8 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B8/B8.sorted.mdup.bam \
  OUTPUT=metrics/B8/B8.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.B8.4c0ed8153fdb2f4a3262e84999293e3d.mugqic.done
)
picard_rna_metrics_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_6_JOB_ID: picard_rna_metrics.C11
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.C11
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.C11.416f884623729c93e0c1a3472b12634f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.C11.416f884623729c93e0c1a3472b12634f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/C11 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/C11/C11.sorted.mdup.bam \
  OUTPUT=metrics/C11/C11 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C11/C11.sorted.mdup.bam \
  OUTPUT=metrics/C11/C11.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.C11.416f884623729c93e0c1a3472b12634f.mugqic.done
)
picard_rna_metrics_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_7_JOB_ID: picard_rna_metrics.C12
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.C12
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.C12.3331fec551a00c0cba77018c2ae20f38.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.C12.3331fec551a00c0cba77018c2ae20f38.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/C12 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/C12/C12.sorted.mdup.bam \
  OUTPUT=metrics/C12/C12 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C12/C12.sorted.mdup.bam \
  OUTPUT=metrics/C12/C12.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.C12.3331fec551a00c0cba77018c2ae20f38.mugqic.done
)
picard_rna_metrics_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_8_JOB_ID: picard_rna_metrics.C4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.C4
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.C4.c4aa50f9d6085003dbe3d484ee448e92.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.C4.c4aa50f9d6085003dbe3d484ee448e92.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/C4 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/C4/C4.sorted.mdup.bam \
  OUTPUT=metrics/C4/C4 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C4/C4.sorted.mdup.bam \
  OUTPUT=metrics/C4/C4.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.C4.c4aa50f9d6085003dbe3d484ee448e92.mugqic.done
)
picard_rna_metrics_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_9_JOB_ID: picard_rna_metrics.C5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.C5
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.C5.e96d1d8e610762f10154dd37b1e25e2d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.C5.e96d1d8e610762f10154dd37b1e25e2d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/C5 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/C5/C5.sorted.mdup.bam \
  OUTPUT=metrics/C5/C5 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C5/C5.sorted.mdup.bam \
  OUTPUT=metrics/C5/C5.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.C5.e96d1d8e610762f10154dd37b1e25e2d.mugqic.done
)
picard_rna_metrics_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_10_JOB_ID: picard_rna_metrics.C9
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.C9
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.C9.7c631c7f12e09d53ab8536755ad2763f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.C9.7c631c7f12e09d53ab8536755ad2763f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/C9 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/C9/C9.sorted.mdup.bam \
  OUTPUT=metrics/C9/C9 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C9/C9.sorted.mdup.bam \
  OUTPUT=metrics/C9/C9.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.C9.7c631c7f12e09d53ab8536755ad2763f.mugqic.done
)
picard_rna_metrics_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_11_JOB_ID: picard_rna_metrics.D12
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.D12
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.D12.2f3e75812a530bbec17a56202471ffbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.D12.2f3e75812a530bbec17a56202471ffbf.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/D12 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/D12/D12.sorted.mdup.bam \
  OUTPUT=metrics/D12/D12 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D12/D12.sorted.mdup.bam \
  OUTPUT=metrics/D12/D12.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.D12.2f3e75812a530bbec17a56202471ffbf.mugqic.done
)
picard_rna_metrics_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_12_JOB_ID: picard_rna_metrics.D2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.D2
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.D2.f8a23cdb6bd8398fa1baba13acfc60a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.D2.f8a23cdb6bd8398fa1baba13acfc60a6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/D2 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/D2/D2.sorted.mdup.bam \
  OUTPUT=metrics/D2/D2 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D2/D2.sorted.mdup.bam \
  OUTPUT=metrics/D2/D2.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.D2.f8a23cdb6bd8398fa1baba13acfc60a6.mugqic.done
)
picard_rna_metrics_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_13_JOB_ID: picard_rna_metrics.D3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.D3
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.D3.ea621178c15c77ae25e5715341d69608.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.D3.ea621178c15c77ae25e5715341d69608.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/D3 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/D3/D3.sorted.mdup.bam \
  OUTPUT=metrics/D3/D3 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D3/D3.sorted.mdup.bam \
  OUTPUT=metrics/D3/D3.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.D3.ea621178c15c77ae25e5715341d69608.mugqic.done
)
picard_rna_metrics_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_14_JOB_ID: picard_rna_metrics.D4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.D4
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.D4.87dcf90df0a152ecf1fd9fd62b1e34f6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.D4.87dcf90df0a152ecf1fd9fd62b1e34f6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/D4 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/D4/D4.sorted.mdup.bam \
  OUTPUT=metrics/D4/D4 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D4/D4.sorted.mdup.bam \
  OUTPUT=metrics/D4/D4.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.D4.87dcf90df0a152ecf1fd9fd62b1e34f6.mugqic.done
)
picard_rna_metrics_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_15_JOB_ID: picard_rna_metrics.D8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.D8
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.D8.bf9f2c3f8040346dadf55adda190d935.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.D8.bf9f2c3f8040346dadf55adda190d935.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/D8 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/D8/D8.sorted.mdup.bam \
  OUTPUT=metrics/D8/D8 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D8/D8.sorted.mdup.bam \
  OUTPUT=metrics/D8/D8.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.D8.bf9f2c3f8040346dadf55adda190d935.mugqic.done
)
picard_rna_metrics_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_16_JOB_ID: picard_rna_metrics.E3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.E3
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.E3.c373ca1e969bd50ed842e7e2873fbee1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.E3.c373ca1e969bd50ed842e7e2873fbee1.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/E3 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/E3/E3.sorted.mdup.bam \
  OUTPUT=metrics/E3/E3 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E3/E3.sorted.mdup.bam \
  OUTPUT=metrics/E3/E3.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.E3.c373ca1e969bd50ed842e7e2873fbee1.mugqic.done
)
picard_rna_metrics_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_17_JOB_ID: picard_rna_metrics.E4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.E4
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.E4.f280503a839a1f02f502ccb7c4a2b844.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.E4.f280503a839a1f02f502ccb7c4a2b844.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/E4 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/E4/E4.sorted.mdup.bam \
  OUTPUT=metrics/E4/E4 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E4/E4.sorted.mdup.bam \
  OUTPUT=metrics/E4/E4.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.E4.f280503a839a1f02f502ccb7c4a2b844.mugqic.done
)
picard_rna_metrics_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_18_JOB_ID: picard_rna_metrics.E6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.E6
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.E6.d15a765cb1503ef74232ef663a8f182b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.E6.d15a765cb1503ef74232ef663a8f182b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/E6 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/E6/E6.sorted.mdup.bam \
  OUTPUT=metrics/E6/E6 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E6/E6.sorted.mdup.bam \
  OUTPUT=metrics/E6/E6.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.E6.d15a765cb1503ef74232ef663a8f182b.mugqic.done
)
picard_rna_metrics_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_19_JOB_ID: picard_rna_metrics.E8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.E8
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.E8.30a7598d25ddbb1d0ee9f4d5894aadad.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.E8.30a7598d25ddbb1d0ee9f4d5894aadad.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/E8 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/E8/E8.sorted.mdup.bam \
  OUTPUT=metrics/E8/E8 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E8/E8.sorted.mdup.bam \
  OUTPUT=metrics/E8/E8.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.E8.30a7598d25ddbb1d0ee9f4d5894aadad.mugqic.done
)
picard_rna_metrics_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_20_JOB_ID: picard_rna_metrics.E9
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.E9
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.E9.dcdde9f66b5105fba173b82810868561.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.E9.dcdde9f66b5105fba173b82810868561.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics/E9 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectMultipleMetrics.jar \
  PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
  TMP_DIR=/gs/scratch/$USER \
  REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  INPUT=alignment/E9/E9.sorted.mdup.bam \
  OUTPUT=metrics/E9/E9 \
  MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $PICARD_HOME/CollectRnaSeqMetrics.jar \
  VALIDATION_STRINGENCY=SILENT  \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E9/E9.sorted.mdup.bam \
  OUTPUT=metrics/E9/E9.picard_rna_metrics \
  REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.ref_flat.tsv \
  STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
  MINIMUM_LENGTH=200 \
  REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.E9.dcdde9f66b5105fba173b82810868561.mugqic.done
)
picard_rna_metrics_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_rna_metrics_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: estimate_ribosomal_rna
#-------------------------------------------------------------------------------
STEP=estimate_ribosomal_rna
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_1_JOB_ID: bwa_mem_rRNA.B11_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.B11_RS
JOB_DEPENDENCIES=$star_22_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.B11_RS.f7796d76635fbd553f5c66890c2deebf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.B11_RS.f7796d76635fbd553f5c66890c2deebf.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/B11/B11_RS metrics/B11/B11_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/B11/B11_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:B11_RS	SM:B11	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/B11/B11_RS/B11_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/B11/B11_RS/B11_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/B11/B11_RS/B11_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.B11_RS.f7796d76635fbd553f5c66890c2deebf.mugqic.done
)
estimate_ribosomal_rna_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_2_JOB_ID: bwa_mem_rRNA.B12_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.B12_RS
JOB_DEPENDENCIES=$star_23_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.B12_RS.50edcec9cbf66e2230a9ea9e5365bd11.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.B12_RS.50edcec9cbf66e2230a9ea9e5365bd11.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/B12/B12_RS metrics/B12/B12_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/B12/B12_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:B12_RS	SM:B12	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/B12/B12_RS/B12_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/B12/B12_RS/B12_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/B12/B12_RS/B12_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.B12_RS.50edcec9cbf66e2230a9ea9e5365bd11.mugqic.done
)
estimate_ribosomal_rna_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_3_JOB_ID: bwa_mem_rRNA.B1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.B1_RS
JOB_DEPENDENCIES=$star_24_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.B1_RS.e782266958a25b315128d1a8447099af.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.B1_RS.e782266958a25b315128d1a8447099af.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/B1/B1_RS metrics/B1/B1_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/B1/B1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:B1_RS	SM:B1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/B1/B1_RS/B1_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/B1/B1_RS/B1_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/B1/B1_RS/B1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.B1_RS.e782266958a25b315128d1a8447099af.mugqic.done
)
estimate_ribosomal_rna_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_4_JOB_ID: bwa_mem_rRNA.B6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.B6_RS
JOB_DEPENDENCIES=$star_25_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.B6_RS.f006155ffbab4dbd147894802856f41c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.B6_RS.f006155ffbab4dbd147894802856f41c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/B6/B6_RS metrics/B6/B6_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/B6/B6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:B6_RS	SM:B6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/B6/B6_RS/B6_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/B6/B6_RS/B6_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/B6/B6_RS/B6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.B6_RS.f006155ffbab4dbd147894802856f41c.mugqic.done
)
estimate_ribosomal_rna_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_5_JOB_ID: bwa_mem_rRNA.B8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.B8_RS
JOB_DEPENDENCIES=$star_26_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.B8_RS.822b45a525e9ef313ef4252b7217a4ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.B8_RS.822b45a525e9ef313ef4252b7217a4ba.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/B8/B8_RS metrics/B8/B8_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/B8/B8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:B8_RS	SM:B8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/B8/B8_RS/B8_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/B8/B8_RS/B8_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/B8/B8_RS/B8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.B8_RS.822b45a525e9ef313ef4252b7217a4ba.mugqic.done
)
estimate_ribosomal_rna_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_6_JOB_ID: bwa_mem_rRNA.C11_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.C11_RS
JOB_DEPENDENCIES=$star_27_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.C11_RS.97b1fb968f280f79457956e63765eeb4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.C11_RS.97b1fb968f280f79457956e63765eeb4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/C11/C11_RS metrics/C11/C11_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/C11/C11_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:C11_RS	SM:C11	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/C11/C11_RS/C11_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/C11/C11_RS/C11_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/C11/C11_RS/C11_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.C11_RS.97b1fb968f280f79457956e63765eeb4.mugqic.done
)
estimate_ribosomal_rna_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_7_JOB_ID: bwa_mem_rRNA.C12_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.C12_RS
JOB_DEPENDENCIES=$star_28_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.C12_RS.35af6c4ec2ff7faedff91c1112428454.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.C12_RS.35af6c4ec2ff7faedff91c1112428454.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/C12/C12_RS metrics/C12/C12_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/C12/C12_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:C12_RS	SM:C12	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/C12/C12_RS/C12_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/C12/C12_RS/C12_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/C12/C12_RS/C12_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.C12_RS.35af6c4ec2ff7faedff91c1112428454.mugqic.done
)
estimate_ribosomal_rna_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_8_JOB_ID: bwa_mem_rRNA.C4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.C4_RS
JOB_DEPENDENCIES=$star_29_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.C4_RS.014b888403af7b00c881f882af1ab242.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.C4_RS.014b888403af7b00c881f882af1ab242.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/C4/C4_RS metrics/C4/C4_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/C4/C4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:C4_RS	SM:C4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/C4/C4_RS/C4_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/C4/C4_RS/C4_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/C4/C4_RS/C4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.C4_RS.014b888403af7b00c881f882af1ab242.mugqic.done
)
estimate_ribosomal_rna_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_9_JOB_ID: bwa_mem_rRNA.C5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.C5_RS
JOB_DEPENDENCIES=$star_30_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.C5_RS.3fce4698d1b81dd7924d92b080d23fdb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.C5_RS.3fce4698d1b81dd7924d92b080d23fdb.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/C5/C5_RS metrics/C5/C5_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/C5/C5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:C5_RS	SM:C5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/C5/C5_RS/C5_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/C5/C5_RS/C5_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/C5/C5_RS/C5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.C5_RS.3fce4698d1b81dd7924d92b080d23fdb.mugqic.done
)
estimate_ribosomal_rna_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_10_JOB_ID: bwa_mem_rRNA.C9_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.C9_RS
JOB_DEPENDENCIES=$star_31_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.C9_RS.2db336c25a787d5d53998ffcbadbd31e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.C9_RS.2db336c25a787d5d53998ffcbadbd31e.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/C9/C9_RS metrics/C9/C9_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/C9/C9_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:C9_RS	SM:C9	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/C9/C9_RS/C9_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/C9/C9_RS/C9_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/C9/C9_RS/C9_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.C9_RS.2db336c25a787d5d53998ffcbadbd31e.mugqic.done
)
estimate_ribosomal_rna_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_11_JOB_ID: bwa_mem_rRNA.D12_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.D12_RS
JOB_DEPENDENCIES=$star_32_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.D12_RS.52d1c70a6f2b1f62d7cc96854771c29f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.D12_RS.52d1c70a6f2b1f62d7cc96854771c29f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/D12/D12_RS metrics/D12/D12_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/D12/D12_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:D12_RS	SM:D12	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/D12/D12_RS/D12_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/D12/D12_RS/D12_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/D12/D12_RS/D12_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.D12_RS.52d1c70a6f2b1f62d7cc96854771c29f.mugqic.done
)
estimate_ribosomal_rna_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_12_JOB_ID: bwa_mem_rRNA.D2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.D2_RS
JOB_DEPENDENCIES=$star_33_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.D2_RS.2c2fd6c214738872b492a730d25eb0d5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.D2_RS.2c2fd6c214738872b492a730d25eb0d5.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/D2/D2_RS metrics/D2/D2_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/D2/D2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:D2_RS	SM:D2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/D2/D2_RS/D2_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/D2/D2_RS/D2_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/D2/D2_RS/D2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.D2_RS.2c2fd6c214738872b492a730d25eb0d5.mugqic.done
)
estimate_ribosomal_rna_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_13_JOB_ID: bwa_mem_rRNA.D3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.D3_RS
JOB_DEPENDENCIES=$star_34_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.D3_RS.f8a8d630dc2874ccbe80fcdca7b99418.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.D3_RS.f8a8d630dc2874ccbe80fcdca7b99418.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/D3/D3_RS metrics/D3/D3_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/D3/D3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:D3_RS	SM:D3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/D3/D3_RS/D3_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/D3/D3_RS/D3_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/D3/D3_RS/D3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.D3_RS.f8a8d630dc2874ccbe80fcdca7b99418.mugqic.done
)
estimate_ribosomal_rna_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_14_JOB_ID: bwa_mem_rRNA.D4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.D4_RS
JOB_DEPENDENCIES=$star_35_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.D4_RS.48bf22ef869d0a4dd9f825aa33611fe8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.D4_RS.48bf22ef869d0a4dd9f825aa33611fe8.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/D4/D4_RS metrics/D4/D4_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/D4/D4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:D4_RS	SM:D4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/D4/D4_RS/D4_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/D4/D4_RS/D4_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/D4/D4_RS/D4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.D4_RS.48bf22ef869d0a4dd9f825aa33611fe8.mugqic.done
)
estimate_ribosomal_rna_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_15_JOB_ID: bwa_mem_rRNA.D8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.D8_RS
JOB_DEPENDENCIES=$star_36_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.D8_RS.3bf15727ea0106cd79045cb23aafc3aa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.D8_RS.3bf15727ea0106cd79045cb23aafc3aa.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/D8/D8_RS metrics/D8/D8_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/D8/D8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:D8_RS	SM:D8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/D8/D8_RS/D8_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/D8/D8_RS/D8_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/D8/D8_RS/D8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.D8_RS.3bf15727ea0106cd79045cb23aafc3aa.mugqic.done
)
estimate_ribosomal_rna_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_16_JOB_ID: bwa_mem_rRNA.E3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.E3_RS
JOB_DEPENDENCIES=$star_37_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.E3_RS.86af420a1e8d65858c12162bc7a172a3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.E3_RS.86af420a1e8d65858c12162bc7a172a3.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/E3/E3_RS metrics/E3/E3_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/E3/E3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:E3_RS	SM:E3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/E3/E3_RS/E3_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/E3/E3_RS/E3_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/E3/E3_RS/E3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.E3_RS.86af420a1e8d65858c12162bc7a172a3.mugqic.done
)
estimate_ribosomal_rna_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_17_JOB_ID: bwa_mem_rRNA.E4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.E4_RS
JOB_DEPENDENCIES=$star_38_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.E4_RS.627a5fb27401d9af6d1043c3af4cb2f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.E4_RS.627a5fb27401d9af6d1043c3af4cb2f9.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/E4/E4_RS metrics/E4/E4_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/E4/E4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:E4_RS	SM:E4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/E4/E4_RS/E4_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/E4/E4_RS/E4_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/E4/E4_RS/E4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.E4_RS.627a5fb27401d9af6d1043c3af4cb2f9.mugqic.done
)
estimate_ribosomal_rna_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_18_JOB_ID: bwa_mem_rRNA.E6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.E6_RS
JOB_DEPENDENCIES=$star_39_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.E6_RS.06a367f6e9e35e0cbc118790e4392713.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.E6_RS.06a367f6e9e35e0cbc118790e4392713.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/E6/E6_RS metrics/E6/E6_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/E6/E6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:E6_RS	SM:E6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/E6/E6_RS/E6_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/E6/E6_RS/E6_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/E6/E6_RS/E6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.E6_RS.06a367f6e9e35e0cbc118790e4392713.mugqic.done
)
estimate_ribosomal_rna_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_19_JOB_ID: bwa_mem_rRNA.E8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.E8_RS
JOB_DEPENDENCIES=$star_40_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.E8_RS.9545ba28b21a5ba489b1d22080ef5a48.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.E8_RS.9545ba28b21a5ba489b1d22080ef5a48.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/E8/E8_RS metrics/E8/E8_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/E8/E8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:E8_RS	SM:E8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/E8/E8_RS/E8_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/E8/E8_RS/E8_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/E8/E8_RS/E8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.E8_RS.9545ba28b21a5ba489b1d22080ef5a48.mugqic.done
)
estimate_ribosomal_rna_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_20_JOB_ID: bwa_mem_rRNA.E9_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.E9_RS
JOB_DEPENDENCIES=$star_41_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.E9_RS.546ccce68ad85d7926d5ed8ad9ea328a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.E9_RS.546ccce68ad85d7926d5ed8ad9ea328a.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 && \
mkdir -p alignment/E9/E9_RS metrics/E9/E9_RS && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/E9/E9_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:E9_RS	SM:E9	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/rrna_bwa_index/Mus_musculus.mm10.UCSC2012-02-07.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/E9/E9_RS/E9_RSrRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/E9/E9_RS/E9_RSrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  -o metrics/E9/E9_RS/E9_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.E9_RS.546ccce68ad85d7926d5ed8ad9ea328a.mugqic.done
)
estimate_ribosomal_rna_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: bam_hard_clip
#-------------------------------------------------------------------------------
STEP=bam_hard_clip
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_1_JOB_ID: tuxedo_hard_clip.B11
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.B11
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.B11.5f0d00c55104e95578b1991f18f73d69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.B11.5f0d00c55104e95578b1991f18f73d69.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/B11/B11.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/B11/B11.sorted.mdup.hardClip.bam
tuxedo_hard_clip.B11.5f0d00c55104e95578b1991f18f73d69.mugqic.done
)
bam_hard_clip_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_2_JOB_ID: tuxedo_hard_clip.B12
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.B12
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.B12.e624ad5dad7ae330a422882224e2e63f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.B12.e624ad5dad7ae330a422882224e2e63f.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/B12/B12.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/B12/B12.sorted.mdup.hardClip.bam
tuxedo_hard_clip.B12.e624ad5dad7ae330a422882224e2e63f.mugqic.done
)
bam_hard_clip_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_3_JOB_ID: tuxedo_hard_clip.B1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.B1
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.B1.c8d574b589f229d560e3ab7b8732db93.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.B1.c8d574b589f229d560e3ab7b8732db93.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/B1/B1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/B1/B1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.B1.c8d574b589f229d560e3ab7b8732db93.mugqic.done
)
bam_hard_clip_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_4_JOB_ID: tuxedo_hard_clip.B6
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.B6
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.B6.45ffc52103593f698f3bedfb9093ca65.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.B6.45ffc52103593f698f3bedfb9093ca65.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/B6/B6.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/B6/B6.sorted.mdup.hardClip.bam
tuxedo_hard_clip.B6.45ffc52103593f698f3bedfb9093ca65.mugqic.done
)
bam_hard_clip_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_5_JOB_ID: tuxedo_hard_clip.B8
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.B8
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.B8.6eb9752ecfd11384ee40577dea3ef2c0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.B8.6eb9752ecfd11384ee40577dea3ef2c0.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/B8/B8.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/B8/B8.sorted.mdup.hardClip.bam
tuxedo_hard_clip.B8.6eb9752ecfd11384ee40577dea3ef2c0.mugqic.done
)
bam_hard_clip_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_6_JOB_ID: tuxedo_hard_clip.C11
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.C11
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.C11.20d765811e7ada7ad9bc84b30e466d8e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.C11.20d765811e7ada7ad9bc84b30e466d8e.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/C11/C11.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/C11/C11.sorted.mdup.hardClip.bam
tuxedo_hard_clip.C11.20d765811e7ada7ad9bc84b30e466d8e.mugqic.done
)
bam_hard_clip_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_7_JOB_ID: tuxedo_hard_clip.C12
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.C12
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.C12.288f244f8b5f1272e51dadf5cdbf3f96.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.C12.288f244f8b5f1272e51dadf5cdbf3f96.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/C12/C12.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/C12/C12.sorted.mdup.hardClip.bam
tuxedo_hard_clip.C12.288f244f8b5f1272e51dadf5cdbf3f96.mugqic.done
)
bam_hard_clip_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_8_JOB_ID: tuxedo_hard_clip.C4
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.C4
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.C4.c3dbfa4d452d680704f70ea6a13f6e74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.C4.c3dbfa4d452d680704f70ea6a13f6e74.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/C4/C4.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/C4/C4.sorted.mdup.hardClip.bam
tuxedo_hard_clip.C4.c3dbfa4d452d680704f70ea6a13f6e74.mugqic.done
)
bam_hard_clip_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_9_JOB_ID: tuxedo_hard_clip.C5
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.C5
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.C5.064ced14e837cee68db817d4a13353aa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.C5.064ced14e837cee68db817d4a13353aa.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/C5/C5.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/C5/C5.sorted.mdup.hardClip.bam
tuxedo_hard_clip.C5.064ced14e837cee68db817d4a13353aa.mugqic.done
)
bam_hard_clip_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_10_JOB_ID: tuxedo_hard_clip.C9
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.C9
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.C9.bf42f20c6dd5494f09a37f0c7f1c9299.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.C9.bf42f20c6dd5494f09a37f0c7f1c9299.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/C9/C9.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/C9/C9.sorted.mdup.hardClip.bam
tuxedo_hard_clip.C9.bf42f20c6dd5494f09a37f0c7f1c9299.mugqic.done
)
bam_hard_clip_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_11_JOB_ID: tuxedo_hard_clip.D12
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.D12
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.D12.32b49874404eca05346e323ee490a15d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.D12.32b49874404eca05346e323ee490a15d.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/D12/D12.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/D12/D12.sorted.mdup.hardClip.bam
tuxedo_hard_clip.D12.32b49874404eca05346e323ee490a15d.mugqic.done
)
bam_hard_clip_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_12_JOB_ID: tuxedo_hard_clip.D2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.D2
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.D2.3d69f229aa9cfd8ed6db3eb9dcc1e0a5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.D2.3d69f229aa9cfd8ed6db3eb9dcc1e0a5.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/D2/D2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/D2/D2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.D2.3d69f229aa9cfd8ed6db3eb9dcc1e0a5.mugqic.done
)
bam_hard_clip_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_13_JOB_ID: tuxedo_hard_clip.D3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.D3
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.D3.6fd7f1580485b2cff2f11deab787207f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.D3.6fd7f1580485b2cff2f11deab787207f.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/D3/D3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/D3/D3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.D3.6fd7f1580485b2cff2f11deab787207f.mugqic.done
)
bam_hard_clip_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_14_JOB_ID: tuxedo_hard_clip.D4
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.D4
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.D4.2794ac3c7428d5fcee6a446cae5cddb4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.D4.2794ac3c7428d5fcee6a446cae5cddb4.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/D4/D4.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/D4/D4.sorted.mdup.hardClip.bam
tuxedo_hard_clip.D4.2794ac3c7428d5fcee6a446cae5cddb4.mugqic.done
)
bam_hard_clip_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_15_JOB_ID: tuxedo_hard_clip.D8
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.D8
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.D8.28b6976bd8b327d5b9465806b6959fa7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.D8.28b6976bd8b327d5b9465806b6959fa7.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/D8/D8.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/D8/D8.sorted.mdup.hardClip.bam
tuxedo_hard_clip.D8.28b6976bd8b327d5b9465806b6959fa7.mugqic.done
)
bam_hard_clip_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_16_JOB_ID: tuxedo_hard_clip.E3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.E3
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.E3.80e7d7251c268ad5c51cd215ecd7922f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.E3.80e7d7251c268ad5c51cd215ecd7922f.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/E3/E3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/E3/E3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.E3.80e7d7251c268ad5c51cd215ecd7922f.mugqic.done
)
bam_hard_clip_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_17_JOB_ID: tuxedo_hard_clip.E4
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.E4
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.E4.838a96401f109fc109987096cdda8ae1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.E4.838a96401f109fc109987096cdda8ae1.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/E4/E4.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/E4/E4.sorted.mdup.hardClip.bam
tuxedo_hard_clip.E4.838a96401f109fc109987096cdda8ae1.mugqic.done
)
bam_hard_clip_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_18_JOB_ID: tuxedo_hard_clip.E6
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.E6
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.E6.4ba24631fe0c9242cad4fc02be2ff14a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.E6.4ba24631fe0c9242cad4fc02be2ff14a.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/E6/E6.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/E6/E6.sorted.mdup.hardClip.bam
tuxedo_hard_clip.E6.4ba24631fe0c9242cad4fc02be2ff14a.mugqic.done
)
bam_hard_clip_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_19_JOB_ID: tuxedo_hard_clip.E8
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.E8
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.E8.9ed0a6aff7fda38721bc996c50ea294f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.E8.9ed0a6aff7fda38721bc996c50ea294f.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/E8/E8.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/E8/E8.sorted.mdup.hardClip.bam
tuxedo_hard_clip.E8.9ed0a6aff7fda38721bc996c50ea294f.mugqic.done
)
bam_hard_clip_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_20_JOB_ID: tuxedo_hard_clip.E9
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.E9
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.E9.88c9ef47a62e5b85ce0d42f2d748e7dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.E9.88c9ef47a62e5b85ce0d42f2d748e7dc.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -h \
  alignment/E9/E9.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/E9/E9.sorted.mdup.hardClip.bam
tuxedo_hard_clip.E9.88c9ef47a62e5b85ce0d42f2d748e7dc.mugqic.done
)
bam_hard_clip_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=4 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bam_hard_clip_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: rnaseqc
#-------------------------------------------------------------------------------
STEP=rnaseqc
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: rnaseqc_1_JOB_ID: rnaseqc
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID:$picard_mark_duplicates_11_JOB_ID:$picard_mark_duplicates_12_JOB_ID:$picard_mark_duplicates_13_JOB_ID:$picard_mark_duplicates_14_JOB_ID:$picard_mark_duplicates_15_JOB_ID:$picard_mark_duplicates_16_JOB_ID:$picard_mark_duplicates_17_JOB_ID:$picard_mark_duplicates_18_JOB_ID:$picard_mark_duplicates_19_JOB_ID:$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc.1f152ceeef168700c2384063819d2b4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc.1f152ceeef168700c2384063819d2b4f.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bwa/0.7.12 mugqic/rnaseqc/1.1.8 && \
mkdir -p metrics/rnaseqRep && \
echo "Sample	BamFile	Note
B11	alignment/B11/B11.sorted.mdup.bam	RNAseq
B12	alignment/B12/B12.sorted.mdup.bam	RNAseq
B1	alignment/B1/B1.sorted.mdup.bam	RNAseq
B6	alignment/B6/B6.sorted.mdup.bam	RNAseq
B8	alignment/B8/B8.sorted.mdup.bam	RNAseq
C11	alignment/C11/C11.sorted.mdup.bam	RNAseq
C12	alignment/C12/C12.sorted.mdup.bam	RNAseq
C4	alignment/C4/C4.sorted.mdup.bam	RNAseq
C5	alignment/C5/C5.sorted.mdup.bam	RNAseq
C9	alignment/C9/C9.sorted.mdup.bam	RNAseq
D12	alignment/D12/D12.sorted.mdup.bam	RNAseq
D2	alignment/D2/D2.sorted.mdup.bam	RNAseq
D3	alignment/D3/D3.sorted.mdup.bam	RNAseq
D4	alignment/D4/D4.sorted.mdup.bam	RNAseq
D8	alignment/D8/D8.sorted.mdup.bam	RNAseq
E3	alignment/E3/E3.sorted.mdup.bam	RNAseq
E4	alignment/E4/E4.sorted.mdup.bam	RNAseq
E6	alignment/E6/E6.sorted.mdup.bam	RNAseq
E8	alignment/E8/E8.sorted.mdup.bam	RNAseq
E9	alignment/E9/E9.sorted.mdup.bam	RNAseq" \
  > alignment/rnaseqc.samples.txt && \
touch dummy_rRNA.fa && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=4 -Xmx27G -jar $RNASEQC_JAR \
  -n 1000 \
  -o metrics/rnaseqRep \
  -r /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  -s alignment/rnaseqc.samples.txt \
  -t /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.transcript_id.gtf \
  -ttype 2\
  -BWArRNA dummy_rRNA.fa && \
zip -r metrics/rnaseqRep.zip metrics/rnaseqRep
rnaseqc.1f152ceeef168700c2384063819d2b4f.mugqic.done
)
rnaseqc_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:00 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$rnaseqc_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: rnaseqc_2_JOB_ID: rnaseqc_report
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc_report.0ce09278d7999a723f6af973c9b4f4a7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc_report.0ce09278d7999a723f6af973c9b4f4a7.mugqic.done'
module load mugqic/python/2.7.11 mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep.zip report/reportRNAseqQC.zip && \
python -c 'import csv; csv_in = csv.DictReader(open("metrics/rnaseqRep/metrics.tsv"), delimiter="	")
print "	".join(["Sample", "Aligned Reads", "Alternative Alignments", "%", "rRNA Reads", "Coverage", "Exonic Rate", "Genes"])
print "\n".join(["	".join([
    line["Sample"],
    line["Mapped"],
    line["Alternative Aligments"],
    str(float(line["Alternative Aligments"]) / float(line["Mapped"]) * 100),
    line["rRNA"],
    line["Mean Per Base Cov."],
    line["Exonic Rate"],
    line["Genes Detected"]
]) for line in csv_in])' \
  > report/trimAlignmentTable.tsv.tmp && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F"	" 'FNR==NR{raw_reads[$1]=$2; surviving_reads[$1]=$3; surviving_pct[$1]=$4; next}{OFS="	"; if ($2=="Aligned Reads"){surviving_pct[$1]="%"; aligned_pct="%"; rrna_pct="%"} else {aligned_pct=($2 / surviving_reads[$1] * 100); rrna_pct=($5 / surviving_reads[$1] * 100)}; printf $1"	"raw_reads[$1]"	"surviving_reads[$1]"	"surviving_pct[$1]"	"$2"	"aligned_pct"	"$3"	"$4"	"$5"	"rrna_pct; for (i = 6; i<= NF; i++) {printf "	"$i}; print ""}' \
  metrics/trimSampleTable.tsv \
  report/trimAlignmentTable.tsv.tmp \
  > report/trimAlignmentTable.tsv
else
  cp report/trimAlignmentTable.tsv.tmp report/trimAlignmentTable.tsv
fi && \
rm report/trimAlignmentTable.tsv.tmp && \
trim_alignment_table_md=`if [[ -f metrics/trimSampleTable.tsv ]] ; then cut -f1-13 report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8), sprintf("%\47d", $9), sprintf("%.1f", $10), sprintf("%.2f", $11), sprintf("%.2f", $12), sprintf("%\47d", $13)}}' ; else cat report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.2f", $6), sprintf("%.2f", $7), $8}}' ; fi`
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.rnaseqc.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.rnaseqc.md \
  --variable trim_alignment_table="$trim_alignment_table_md" \
  --to markdown \
  > report/RnaSeq.rnaseqc.md
rnaseqc_report.0ce09278d7999a723f6af973c9b4f4a7.mugqic.done
)
rnaseqc_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$rnaseqc_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: wiggle
#-------------------------------------------------------------------------------
STEP=wiggle
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: wiggle_1_JOB_ID: wiggle.B11.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B11.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B11.forward_strandspec.a3febf5497c687e49e32061368ebfdaf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B11.forward_strandspec.a3febf5497c687e49e32061368ebfdaf.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/B11/B11.sorted.mdup.bam \
  > alignment/B11/B11.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/B11/B11.sorted.mdup.bam \
  > alignment/B11/B11.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B11/B11.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/B11/B11.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/B11/B11.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B11/B11.sorted.mdup.tmp1.forward.bam alignment/B11/B11.sorted.mdup.tmp2.forward.bam
wiggle.B11.forward_strandspec.a3febf5497c687e49e32061368ebfdaf.mugqic.done
)
wiggle_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_2_JOB_ID: wiggle.B11.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B11.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B11.reverse_strandspec.dab0acf784d3adac5b69bf9a67b5e48e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B11.reverse_strandspec.dab0acf784d3adac5b69bf9a67b5e48e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/B11 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/B11/B11.sorted.mdup.bam \
  > alignment/B11/B11.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/B11/B11.sorted.mdup.bam \
  > alignment/B11/B11.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B11/B11.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/B11/B11.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/B11/B11.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B11/B11.sorted.mdup.tmp1.reverse.bam alignment/B11/B11.sorted.mdup.tmp2.reverse.bam
wiggle.B11.reverse_strandspec.dab0acf784d3adac5b69bf9a67b5e48e.mugqic.done
)
wiggle_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_3_JOB_ID: wiggle.B11.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B11.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B11.forward.2724086abbc981a946c49591edb97060.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B11.forward.2724086abbc981a946c49591edb97060.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B11 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B11/B11.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B11/B11.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B11/B11.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/B11/B11.forward.bedGraph > tracks/B11/B11.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B11/B11.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B11.forward.bw
wiggle.B11.forward.2724086abbc981a946c49591edb97060.mugqic.done
)
wiggle_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_4_JOB_ID: wiggle.B11.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B11.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B11.reverse.42a8edfa2288cb3f09392cb9478a6545.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B11.reverse.42a8edfa2288cb3f09392cb9478a6545.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B11 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B11/B11.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B11/B11.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B11/B11.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/B11/B11.reverse.bedGraph > tracks/B11/B11.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B11/B11.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B11.reverse.bw
wiggle.B11.reverse.42a8edfa2288cb3f09392cb9478a6545.mugqic.done
)
wiggle_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_5_JOB_ID: wiggle.B12.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B12.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B12.forward_strandspec.9d7ffa36df7159ffc92b7b7531c7016d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B12.forward_strandspec.9d7ffa36df7159ffc92b7b7531c7016d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/B12/B12.sorted.mdup.bam \
  > alignment/B12/B12.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/B12/B12.sorted.mdup.bam \
  > alignment/B12/B12.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B12/B12.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/B12/B12.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/B12/B12.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B12/B12.sorted.mdup.tmp1.forward.bam alignment/B12/B12.sorted.mdup.tmp2.forward.bam
wiggle.B12.forward_strandspec.9d7ffa36df7159ffc92b7b7531c7016d.mugqic.done
)
wiggle_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_6_JOB_ID: wiggle.B12.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B12.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B12.reverse_strandspec.8b0db4a26437359b3fbd5069ab8fbbd9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B12.reverse_strandspec.8b0db4a26437359b3fbd5069ab8fbbd9.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/B12 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/B12/B12.sorted.mdup.bam \
  > alignment/B12/B12.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/B12/B12.sorted.mdup.bam \
  > alignment/B12/B12.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B12/B12.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/B12/B12.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/B12/B12.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B12/B12.sorted.mdup.tmp1.reverse.bam alignment/B12/B12.sorted.mdup.tmp2.reverse.bam
wiggle.B12.reverse_strandspec.8b0db4a26437359b3fbd5069ab8fbbd9.mugqic.done
)
wiggle_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_7_JOB_ID: wiggle.B12.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B12.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B12.forward.9050f9513e4f769fc7109ff8e769a6f3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B12.forward.9050f9513e4f769fc7109ff8e769a6f3.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B12/B12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B12/B12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B12/B12.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/B12/B12.forward.bedGraph > tracks/B12/B12.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B12/B12.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B12.forward.bw
wiggle.B12.forward.9050f9513e4f769fc7109ff8e769a6f3.mugqic.done
)
wiggle_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_8_JOB_ID: wiggle.B12.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B12.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B12.reverse.c9adc7081d716b5ed9ef6310087b1321.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B12.reverse.c9adc7081d716b5ed9ef6310087b1321.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B12/B12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B12/B12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B12/B12.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/B12/B12.reverse.bedGraph > tracks/B12/B12.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B12/B12.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B12.reverse.bw
wiggle.B12.reverse.c9adc7081d716b5ed9ef6310087b1321.mugqic.done
)
wiggle_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_9_JOB_ID: wiggle.B1.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B1.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B1.forward_strandspec.bf56596af35e50941db61370c0a14dbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B1.forward_strandspec.bf56596af35e50941db61370c0a14dbf.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/B1/B1.sorted.mdup.bam \
  > alignment/B1/B1.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/B1/B1.sorted.mdup.bam \
  > alignment/B1/B1.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B1/B1.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/B1/B1.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/B1/B1.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B1/B1.sorted.mdup.tmp1.forward.bam alignment/B1/B1.sorted.mdup.tmp2.forward.bam
wiggle.B1.forward_strandspec.bf56596af35e50941db61370c0a14dbf.mugqic.done
)
wiggle_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_10_JOB_ID: wiggle.B1.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B1.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B1.reverse_strandspec.0b14687575539e2e918653a9b59e6e62.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B1.reverse_strandspec.0b14687575539e2e918653a9b59e6e62.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/B1 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/B1/B1.sorted.mdup.bam \
  > alignment/B1/B1.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/B1/B1.sorted.mdup.bam \
  > alignment/B1/B1.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B1/B1.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/B1/B1.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/B1/B1.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B1/B1.sorted.mdup.tmp1.reverse.bam alignment/B1/B1.sorted.mdup.tmp2.reverse.bam
wiggle.B1.reverse_strandspec.0b14687575539e2e918653a9b59e6e62.mugqic.done
)
wiggle_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_11_JOB_ID: wiggle.B1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B1.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B1.forward.0c080590dd9422740287cb59a52e6f5b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B1.forward.0c080590dd9422740287cb59a52e6f5b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B1 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B1/B1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B1/B1.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B1/B1.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/B1/B1.forward.bedGraph > tracks/B1/B1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B1/B1.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B1.forward.bw
wiggle.B1.forward.0c080590dd9422740287cb59a52e6f5b.mugqic.done
)
wiggle_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_12_JOB_ID: wiggle.B1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B1.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B1.reverse.1ecb44cb7b5933517058fabcb41dcb4b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B1.reverse.1ecb44cb7b5933517058fabcb41dcb4b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B1 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B1/B1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B1/B1.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B1/B1.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/B1/B1.reverse.bedGraph > tracks/B1/B1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B1/B1.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B1.reverse.bw
wiggle.B1.reverse.1ecb44cb7b5933517058fabcb41dcb4b.mugqic.done
)
wiggle_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_13_JOB_ID: wiggle.B6.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B6.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B6.forward_strandspec.0a6bff377fb30eefeb15cb7dfc03be11.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B6.forward_strandspec.0a6bff377fb30eefeb15cb7dfc03be11.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/B6/B6.sorted.mdup.bam \
  > alignment/B6/B6.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/B6/B6.sorted.mdup.bam \
  > alignment/B6/B6.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B6/B6.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/B6/B6.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/B6/B6.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B6/B6.sorted.mdup.tmp1.forward.bam alignment/B6/B6.sorted.mdup.tmp2.forward.bam
wiggle.B6.forward_strandspec.0a6bff377fb30eefeb15cb7dfc03be11.mugqic.done
)
wiggle_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_14_JOB_ID: wiggle.B6.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B6.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B6.reverse_strandspec.d2b578400052aefea744888fdb690e3f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B6.reverse_strandspec.d2b578400052aefea744888fdb690e3f.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/B6 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/B6/B6.sorted.mdup.bam \
  > alignment/B6/B6.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/B6/B6.sorted.mdup.bam \
  > alignment/B6/B6.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B6/B6.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/B6/B6.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/B6/B6.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B6/B6.sorted.mdup.tmp1.reverse.bam alignment/B6/B6.sorted.mdup.tmp2.reverse.bam
wiggle.B6.reverse_strandspec.d2b578400052aefea744888fdb690e3f.mugqic.done
)
wiggle_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_15_JOB_ID: wiggle.B6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B6.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B6.forward.025dff9de3efc147af1bdf7e03456ecb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B6.forward.025dff9de3efc147af1bdf7e03456ecb.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B6 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B6/B6.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B6/B6.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B6/B6.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/B6/B6.forward.bedGraph > tracks/B6/B6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B6/B6.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B6.forward.bw
wiggle.B6.forward.025dff9de3efc147af1bdf7e03456ecb.mugqic.done
)
wiggle_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_16_JOB_ID: wiggle.B6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B6.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B6.reverse.4d7fe934a8ef06aa992f55b483ec31c1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B6.reverse.4d7fe934a8ef06aa992f55b483ec31c1.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B6 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B6/B6.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B6/B6.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B6/B6.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/B6/B6.reverse.bedGraph > tracks/B6/B6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B6/B6.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B6.reverse.bw
wiggle.B6.reverse.4d7fe934a8ef06aa992f55b483ec31c1.mugqic.done
)
wiggle_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_17_JOB_ID: wiggle.B8.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B8.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B8.forward_strandspec.ecb502a082dcecbb4336f25f16db9811.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B8.forward_strandspec.ecb502a082dcecbb4336f25f16db9811.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/B8/B8.sorted.mdup.bam \
  > alignment/B8/B8.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/B8/B8.sorted.mdup.bam \
  > alignment/B8/B8.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B8/B8.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/B8/B8.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/B8/B8.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B8/B8.sorted.mdup.tmp1.forward.bam alignment/B8/B8.sorted.mdup.tmp2.forward.bam
wiggle.B8.forward_strandspec.ecb502a082dcecbb4336f25f16db9811.mugqic.done
)
wiggle_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_18_JOB_ID: wiggle.B8.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B8.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B8.reverse_strandspec.12cd9e891fbc2d56bc5d6448c7e2454c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B8.reverse_strandspec.12cd9e891fbc2d56bc5d6448c7e2454c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/B8 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/B8/B8.sorted.mdup.bam \
  > alignment/B8/B8.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/B8/B8.sorted.mdup.bam \
  > alignment/B8/B8.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/B8/B8.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/B8/B8.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/B8/B8.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/B8/B8.sorted.mdup.tmp1.reverse.bam alignment/B8/B8.sorted.mdup.tmp2.reverse.bam
wiggle.B8.reverse_strandspec.12cd9e891fbc2d56bc5d6448c7e2454c.mugqic.done
)
wiggle_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_19_JOB_ID: wiggle.B8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B8.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B8.forward.6c1cc7bcd21c51cc678ff46a277751b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B8.forward.6c1cc7bcd21c51cc678ff46a277751b4.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B8/B8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B8/B8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B8/B8.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/B8/B8.forward.bedGraph > tracks/B8/B8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B8/B8.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B8.forward.bw
wiggle.B8.forward.6c1cc7bcd21c51cc678ff46a277751b4.mugqic.done
)
wiggle_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_20_JOB_ID: wiggle.B8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.B8.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.B8.reverse.5a504b9717d2d43281e3aa9c23254bc2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.B8.reverse.5a504b9717d2d43281e3aa9c23254bc2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/B8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/B8/B8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/B8/B8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/B8/B8.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/B8/B8.reverse.bedGraph > tracks/B8/B8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/B8/B8.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/B8.reverse.bw
wiggle.B8.reverse.5a504b9717d2d43281e3aa9c23254bc2.mugqic.done
)
wiggle_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_21_JOB_ID: wiggle.C11.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C11.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C11.forward_strandspec.c276d7367eece61936d33fa9eb94e6b9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C11.forward_strandspec.c276d7367eece61936d33fa9eb94e6b9.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/C11/C11.sorted.mdup.bam \
  > alignment/C11/C11.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/C11/C11.sorted.mdup.bam \
  > alignment/C11/C11.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C11/C11.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/C11/C11.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/C11/C11.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C11/C11.sorted.mdup.tmp1.forward.bam alignment/C11/C11.sorted.mdup.tmp2.forward.bam
wiggle.C11.forward_strandspec.c276d7367eece61936d33fa9eb94e6b9.mugqic.done
)
wiggle_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_22_JOB_ID: wiggle.C11.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C11.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C11.reverse_strandspec.bc06bf9cbdb45182810163d30c4999c7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C11.reverse_strandspec.bc06bf9cbdb45182810163d30c4999c7.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/C11 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/C11/C11.sorted.mdup.bam \
  > alignment/C11/C11.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/C11/C11.sorted.mdup.bam \
  > alignment/C11/C11.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C11/C11.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/C11/C11.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/C11/C11.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C11/C11.sorted.mdup.tmp1.reverse.bam alignment/C11/C11.sorted.mdup.tmp2.reverse.bam
wiggle.C11.reverse_strandspec.bc06bf9cbdb45182810163d30c4999c7.mugqic.done
)
wiggle_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_23_JOB_ID: wiggle.C11.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C11.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C11.forward.0f18b80c09c1d1a97f8e82219591eb2a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C11.forward.0f18b80c09c1d1a97f8e82219591eb2a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C11 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C11/C11.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C11/C11.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C11/C11.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/C11/C11.forward.bedGraph > tracks/C11/C11.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C11/C11.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C11.forward.bw
wiggle.C11.forward.0f18b80c09c1d1a97f8e82219591eb2a.mugqic.done
)
wiggle_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_24_JOB_ID: wiggle.C11.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C11.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C11.reverse.660349e1ef5e2f5afe10ed76f4883a74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C11.reverse.660349e1ef5e2f5afe10ed76f4883a74.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C11 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C11/C11.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C11/C11.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C11/C11.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/C11/C11.reverse.bedGraph > tracks/C11/C11.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C11/C11.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C11.reverse.bw
wiggle.C11.reverse.660349e1ef5e2f5afe10ed76f4883a74.mugqic.done
)
wiggle_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_25_JOB_ID: wiggle.C12.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C12.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C12.forward_strandspec.c515090bfee240e0a7e411ddec6a258a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C12.forward_strandspec.c515090bfee240e0a7e411ddec6a258a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/C12/C12.sorted.mdup.bam \
  > alignment/C12/C12.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/C12/C12.sorted.mdup.bam \
  > alignment/C12/C12.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C12/C12.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/C12/C12.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/C12/C12.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C12/C12.sorted.mdup.tmp1.forward.bam alignment/C12/C12.sorted.mdup.tmp2.forward.bam
wiggle.C12.forward_strandspec.c515090bfee240e0a7e411ddec6a258a.mugqic.done
)
wiggle_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_26_JOB_ID: wiggle.C12.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C12.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C12.reverse_strandspec.76ed3dc0a9ae7d9f144f97f1371a80e7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C12.reverse_strandspec.76ed3dc0a9ae7d9f144f97f1371a80e7.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/C12 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/C12/C12.sorted.mdup.bam \
  > alignment/C12/C12.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/C12/C12.sorted.mdup.bam \
  > alignment/C12/C12.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C12/C12.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/C12/C12.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/C12/C12.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C12/C12.sorted.mdup.tmp1.reverse.bam alignment/C12/C12.sorted.mdup.tmp2.reverse.bam
wiggle.C12.reverse_strandspec.76ed3dc0a9ae7d9f144f97f1371a80e7.mugqic.done
)
wiggle_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_27_JOB_ID: wiggle.C12.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C12.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C12.forward.d3e196ee7c96b1876c9409e56b654de2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C12.forward.d3e196ee7c96b1876c9409e56b654de2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C12/C12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C12/C12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C12/C12.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/C12/C12.forward.bedGraph > tracks/C12/C12.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C12/C12.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C12.forward.bw
wiggle.C12.forward.d3e196ee7c96b1876c9409e56b654de2.mugqic.done
)
wiggle_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_28_JOB_ID: wiggle.C12.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C12.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C12.reverse.b31727b0d98b40f74666458b934edaaf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C12.reverse.b31727b0d98b40f74666458b934edaaf.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C12/C12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C12/C12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C12/C12.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/C12/C12.reverse.bedGraph > tracks/C12/C12.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C12/C12.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C12.reverse.bw
wiggle.C12.reverse.b31727b0d98b40f74666458b934edaaf.mugqic.done
)
wiggle_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_29_JOB_ID: wiggle.C4.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C4.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C4.forward_strandspec.215cbdb17c73af63ed89665e7ca34beb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C4.forward_strandspec.215cbdb17c73af63ed89665e7ca34beb.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/C4/C4.sorted.mdup.bam \
  > alignment/C4/C4.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/C4/C4.sorted.mdup.bam \
  > alignment/C4/C4.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C4/C4.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/C4/C4.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/C4/C4.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C4/C4.sorted.mdup.tmp1.forward.bam alignment/C4/C4.sorted.mdup.tmp2.forward.bam
wiggle.C4.forward_strandspec.215cbdb17c73af63ed89665e7ca34beb.mugqic.done
)
wiggle_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_30_JOB_ID: wiggle.C4.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C4.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C4.reverse_strandspec.4b551403e1fb6b74b08f2a025d6f419f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C4.reverse_strandspec.4b551403e1fb6b74b08f2a025d6f419f.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/C4 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/C4/C4.sorted.mdup.bam \
  > alignment/C4/C4.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/C4/C4.sorted.mdup.bam \
  > alignment/C4/C4.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C4/C4.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/C4/C4.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/C4/C4.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C4/C4.sorted.mdup.tmp1.reverse.bam alignment/C4/C4.sorted.mdup.tmp2.reverse.bam
wiggle.C4.reverse_strandspec.4b551403e1fb6b74b08f2a025d6f419f.mugqic.done
)
wiggle_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_31_JOB_ID: wiggle.C4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C4.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C4.forward.f4de4f862e47261af4221f9e8ad7f822.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C4.forward.f4de4f862e47261af4221f9e8ad7f822.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C4/C4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C4/C4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C4/C4.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/C4/C4.forward.bedGraph > tracks/C4/C4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C4/C4.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C4.forward.bw
wiggle.C4.forward.f4de4f862e47261af4221f9e8ad7f822.mugqic.done
)
wiggle_31_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_32_JOB_ID: wiggle.C4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C4.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C4.reverse.58c16a989866f257764f5fffda9cc47d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C4.reverse.58c16a989866f257764f5fffda9cc47d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C4/C4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C4/C4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C4/C4.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/C4/C4.reverse.bedGraph > tracks/C4/C4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C4/C4.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C4.reverse.bw
wiggle.C4.reverse.58c16a989866f257764f5fffda9cc47d.mugqic.done
)
wiggle_32_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_33_JOB_ID: wiggle.C5.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C5.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C5.forward_strandspec.5588de8e3b219f54466eb434c04cf43e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C5.forward_strandspec.5588de8e3b219f54466eb434c04cf43e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/C5/C5.sorted.mdup.bam \
  > alignment/C5/C5.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/C5/C5.sorted.mdup.bam \
  > alignment/C5/C5.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C5/C5.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/C5/C5.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/C5/C5.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C5/C5.sorted.mdup.tmp1.forward.bam alignment/C5/C5.sorted.mdup.tmp2.forward.bam
wiggle.C5.forward_strandspec.5588de8e3b219f54466eb434c04cf43e.mugqic.done
)
wiggle_33_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_34_JOB_ID: wiggle.C5.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C5.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C5.reverse_strandspec.963dd264c214a889ca9ca871da4e46e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C5.reverse_strandspec.963dd264c214a889ca9ca871da4e46e8.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/C5 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/C5/C5.sorted.mdup.bam \
  > alignment/C5/C5.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/C5/C5.sorted.mdup.bam \
  > alignment/C5/C5.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C5/C5.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/C5/C5.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/C5/C5.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C5/C5.sorted.mdup.tmp1.reverse.bam alignment/C5/C5.sorted.mdup.tmp2.reverse.bam
wiggle.C5.reverse_strandspec.963dd264c214a889ca9ca871da4e46e8.mugqic.done
)
wiggle_34_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_35_JOB_ID: wiggle.C5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C5.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C5.forward.35b87c7e649313cb71e4b887c926bdf0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C5.forward.35b87c7e649313cb71e4b887c926bdf0.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C5 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C5/C5.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C5/C5.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C5/C5.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/C5/C5.forward.bedGraph > tracks/C5/C5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C5/C5.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C5.forward.bw
wiggle.C5.forward.35b87c7e649313cb71e4b887c926bdf0.mugqic.done
)
wiggle_35_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_36_JOB_ID: wiggle.C5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C5.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C5.reverse.75abfc72a892814d82a10a93aad9fc09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C5.reverse.75abfc72a892814d82a10a93aad9fc09.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C5 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C5/C5.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C5/C5.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C5/C5.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/C5/C5.reverse.bedGraph > tracks/C5/C5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C5/C5.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C5.reverse.bw
wiggle.C5.reverse.75abfc72a892814d82a10a93aad9fc09.mugqic.done
)
wiggle_36_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_37_JOB_ID: wiggle.C9.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C9.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C9.forward_strandspec.74df55d9ef935dc1ec3c48da67d68391.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C9.forward_strandspec.74df55d9ef935dc1ec3c48da67d68391.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/C9/C9.sorted.mdup.bam \
  > alignment/C9/C9.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/C9/C9.sorted.mdup.bam \
  > alignment/C9/C9.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C9/C9.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/C9/C9.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/C9/C9.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C9/C9.sorted.mdup.tmp1.forward.bam alignment/C9/C9.sorted.mdup.tmp2.forward.bam
wiggle.C9.forward_strandspec.74df55d9ef935dc1ec3c48da67d68391.mugqic.done
)
wiggle_37_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_38_JOB_ID: wiggle.C9.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C9.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C9.reverse_strandspec.4c1d3f1f63e4969c965e15f293df1ca7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C9.reverse_strandspec.4c1d3f1f63e4969c965e15f293df1ca7.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/C9 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/C9/C9.sorted.mdup.bam \
  > alignment/C9/C9.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/C9/C9.sorted.mdup.bam \
  > alignment/C9/C9.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/C9/C9.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/C9/C9.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/C9/C9.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/C9/C9.sorted.mdup.tmp1.reverse.bam alignment/C9/C9.sorted.mdup.tmp2.reverse.bam
wiggle.C9.reverse_strandspec.4c1d3f1f63e4969c965e15f293df1ca7.mugqic.done
)
wiggle_38_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_39_JOB_ID: wiggle.C9.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C9.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C9.forward.35bcf65280357569a4763d5cb844ea4b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C9.forward.35bcf65280357569a4763d5cb844ea4b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C9 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C9/C9.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C9/C9.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C9/C9.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/C9/C9.forward.bedGraph > tracks/C9/C9.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C9/C9.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C9.forward.bw
wiggle.C9.forward.35bcf65280357569a4763d5cb844ea4b.mugqic.done
)
wiggle_39_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_40_JOB_ID: wiggle.C9.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.C9.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.C9.reverse.c7bc678b2b1686c6b58760e763e1099c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.C9.reverse.c7bc678b2b1686c6b58760e763e1099c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/C9 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/C9/C9.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/C9/C9.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/C9/C9.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/C9/C9.reverse.bedGraph > tracks/C9/C9.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/C9/C9.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/C9.reverse.bw
wiggle.C9.reverse.c7bc678b2b1686c6b58760e763e1099c.mugqic.done
)
wiggle_40_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_41_JOB_ID: wiggle.D12.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D12.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D12.forward_strandspec.4f05e0bf26b7873a3e04e2c3cd8c42ac.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D12.forward_strandspec.4f05e0bf26b7873a3e04e2c3cd8c42ac.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/D12/D12.sorted.mdup.bam \
  > alignment/D12/D12.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/D12/D12.sorted.mdup.bam \
  > alignment/D12/D12.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D12/D12.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/D12/D12.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/D12/D12.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D12/D12.sorted.mdup.tmp1.forward.bam alignment/D12/D12.sorted.mdup.tmp2.forward.bam
wiggle.D12.forward_strandspec.4f05e0bf26b7873a3e04e2c3cd8c42ac.mugqic.done
)
wiggle_41_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_42_JOB_ID: wiggle.D12.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D12.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D12.reverse_strandspec.ce06aaa0d5e5a8a890682f51253186c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D12.reverse_strandspec.ce06aaa0d5e5a8a890682f51253186c6.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/D12 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/D12/D12.sorted.mdup.bam \
  > alignment/D12/D12.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/D12/D12.sorted.mdup.bam \
  > alignment/D12/D12.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D12/D12.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/D12/D12.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/D12/D12.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D12/D12.sorted.mdup.tmp1.reverse.bam alignment/D12/D12.sorted.mdup.tmp2.reverse.bam
wiggle.D12.reverse_strandspec.ce06aaa0d5e5a8a890682f51253186c6.mugqic.done
)
wiggle_42_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_43_JOB_ID: wiggle.D12.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D12.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D12.forward.0ad010c752e51e8b14a6354f6590884e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D12.forward.0ad010c752e51e8b14a6354f6590884e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D12/D12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D12/D12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D12/D12.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/D12/D12.forward.bedGraph > tracks/D12/D12.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D12/D12.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D12.forward.bw
wiggle.D12.forward.0ad010c752e51e8b14a6354f6590884e.mugqic.done
)
wiggle_43_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_44_JOB_ID: wiggle.D12.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D12.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D12.reverse.905d5c2d6188e0fad2f8b94bd45e78c0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D12.reverse.905d5c2d6188e0fad2f8b94bd45e78c0.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D12 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D12/D12.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D12/D12.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D12/D12.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/D12/D12.reverse.bedGraph > tracks/D12/D12.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D12/D12.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D12.reverse.bw
wiggle.D12.reverse.905d5c2d6188e0fad2f8b94bd45e78c0.mugqic.done
)
wiggle_44_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_45_JOB_ID: wiggle.D2.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D2.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D2.forward_strandspec.f814ae80e8d87a8a9a142493861c75a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D2.forward_strandspec.f814ae80e8d87a8a9a142493861c75a2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/D2/D2.sorted.mdup.bam \
  > alignment/D2/D2.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/D2/D2.sorted.mdup.bam \
  > alignment/D2/D2.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D2/D2.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/D2/D2.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/D2/D2.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D2/D2.sorted.mdup.tmp1.forward.bam alignment/D2/D2.sorted.mdup.tmp2.forward.bam
wiggle.D2.forward_strandspec.f814ae80e8d87a8a9a142493861c75a2.mugqic.done
)
wiggle_45_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_46_JOB_ID: wiggle.D2.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D2.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D2.reverse_strandspec.92d562546df39a73b5fe78e68bbbe1c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D2.reverse_strandspec.92d562546df39a73b5fe78e68bbbe1c6.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/D2 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/D2/D2.sorted.mdup.bam \
  > alignment/D2/D2.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/D2/D2.sorted.mdup.bam \
  > alignment/D2/D2.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D2/D2.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/D2/D2.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/D2/D2.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D2/D2.sorted.mdup.tmp1.reverse.bam alignment/D2/D2.sorted.mdup.tmp2.reverse.bam
wiggle.D2.reverse_strandspec.92d562546df39a73b5fe78e68bbbe1c6.mugqic.done
)
wiggle_46_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_47_JOB_ID: wiggle.D2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D2.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D2.forward.819a79b92ce490c50ab20b492907d8c1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D2.forward.819a79b92ce490c50ab20b492907d8c1.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D2 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D2/D2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D2/D2.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D2/D2.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/D2/D2.forward.bedGraph > tracks/D2/D2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D2/D2.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D2.forward.bw
wiggle.D2.forward.819a79b92ce490c50ab20b492907d8c1.mugqic.done
)
wiggle_47_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_48_JOB_ID: wiggle.D2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D2.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D2.reverse.1de0db0b266abade5423d3a7ff60f35d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D2.reverse.1de0db0b266abade5423d3a7ff60f35d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D2 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D2/D2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D2/D2.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D2/D2.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/D2/D2.reverse.bedGraph > tracks/D2/D2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D2/D2.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D2.reverse.bw
wiggle.D2.reverse.1de0db0b266abade5423d3a7ff60f35d.mugqic.done
)
wiggle_48_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_49_JOB_ID: wiggle.D3.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D3.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D3.forward_strandspec.fb5e0a0441c422e780f48830537e6c73.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D3.forward_strandspec.fb5e0a0441c422e780f48830537e6c73.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/D3/D3.sorted.mdup.bam \
  > alignment/D3/D3.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/D3/D3.sorted.mdup.bam \
  > alignment/D3/D3.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D3/D3.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/D3/D3.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/D3/D3.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D3/D3.sorted.mdup.tmp1.forward.bam alignment/D3/D3.sorted.mdup.tmp2.forward.bam
wiggle.D3.forward_strandspec.fb5e0a0441c422e780f48830537e6c73.mugqic.done
)
wiggle_49_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_49_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_50_JOB_ID: wiggle.D3.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D3.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D3.reverse_strandspec.c24e205000bb6a35a9d7cddede80a69b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D3.reverse_strandspec.c24e205000bb6a35a9d7cddede80a69b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/D3 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/D3/D3.sorted.mdup.bam \
  > alignment/D3/D3.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/D3/D3.sorted.mdup.bam \
  > alignment/D3/D3.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D3/D3.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/D3/D3.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/D3/D3.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D3/D3.sorted.mdup.tmp1.reverse.bam alignment/D3/D3.sorted.mdup.tmp2.reverse.bam
wiggle.D3.reverse_strandspec.c24e205000bb6a35a9d7cddede80a69b.mugqic.done
)
wiggle_50_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_50_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_51_JOB_ID: wiggle.D3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D3.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D3.forward.e3c414cce0f791b3e25fff4090092333.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D3.forward.e3c414cce0f791b3e25fff4090092333.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D3 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D3/D3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D3/D3.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D3/D3.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/D3/D3.forward.bedGraph > tracks/D3/D3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D3/D3.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D3.forward.bw
wiggle.D3.forward.e3c414cce0f791b3e25fff4090092333.mugqic.done
)
wiggle_51_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_51_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_52_JOB_ID: wiggle.D3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D3.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D3.reverse.b5e11782fe249b89822d653ed35633c3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D3.reverse.b5e11782fe249b89822d653ed35633c3.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D3 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D3/D3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D3/D3.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D3/D3.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/D3/D3.reverse.bedGraph > tracks/D3/D3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D3/D3.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D3.reverse.bw
wiggle.D3.reverse.b5e11782fe249b89822d653ed35633c3.mugqic.done
)
wiggle_52_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_52_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_53_JOB_ID: wiggle.D4.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D4.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D4.forward_strandspec.442cf13d7092302994d10c24e0fde196.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D4.forward_strandspec.442cf13d7092302994d10c24e0fde196.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/D4/D4.sorted.mdup.bam \
  > alignment/D4/D4.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/D4/D4.sorted.mdup.bam \
  > alignment/D4/D4.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D4/D4.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/D4/D4.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/D4/D4.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D4/D4.sorted.mdup.tmp1.forward.bam alignment/D4/D4.sorted.mdup.tmp2.forward.bam
wiggle.D4.forward_strandspec.442cf13d7092302994d10c24e0fde196.mugqic.done
)
wiggle_53_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_53_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_54_JOB_ID: wiggle.D4.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D4.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D4.reverse_strandspec.02e29da051da5e32ce5d3e7961d4537b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D4.reverse_strandspec.02e29da051da5e32ce5d3e7961d4537b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/D4 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/D4/D4.sorted.mdup.bam \
  > alignment/D4/D4.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/D4/D4.sorted.mdup.bam \
  > alignment/D4/D4.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D4/D4.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/D4/D4.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/D4/D4.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D4/D4.sorted.mdup.tmp1.reverse.bam alignment/D4/D4.sorted.mdup.tmp2.reverse.bam
wiggle.D4.reverse_strandspec.02e29da051da5e32ce5d3e7961d4537b.mugqic.done
)
wiggle_54_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_54_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_55_JOB_ID: wiggle.D4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D4.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D4.forward.c999b2cd56b20687d765679ee4c967b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D4.forward.c999b2cd56b20687d765679ee4c967b3.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D4/D4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D4/D4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D4/D4.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/D4/D4.forward.bedGraph > tracks/D4/D4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D4/D4.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D4.forward.bw
wiggle.D4.forward.c999b2cd56b20687d765679ee4c967b3.mugqic.done
)
wiggle_55_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_55_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_56_JOB_ID: wiggle.D4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D4.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D4.reverse.7d6fc197838c46ee5acc4a5a8a977923.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D4.reverse.7d6fc197838c46ee5acc4a5a8a977923.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D4/D4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D4/D4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D4/D4.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/D4/D4.reverse.bedGraph > tracks/D4/D4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D4/D4.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D4.reverse.bw
wiggle.D4.reverse.7d6fc197838c46ee5acc4a5a8a977923.mugqic.done
)
wiggle_56_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_56_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_57_JOB_ID: wiggle.D8.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D8.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D8.forward_strandspec.ba5244539317cce7cf0d54b625191f6c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D8.forward_strandspec.ba5244539317cce7cf0d54b625191f6c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/D8/D8.sorted.mdup.bam \
  > alignment/D8/D8.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/D8/D8.sorted.mdup.bam \
  > alignment/D8/D8.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D8/D8.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/D8/D8.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/D8/D8.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D8/D8.sorted.mdup.tmp1.forward.bam alignment/D8/D8.sorted.mdup.tmp2.forward.bam
wiggle.D8.forward_strandspec.ba5244539317cce7cf0d54b625191f6c.mugqic.done
)
wiggle_57_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_57_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_58_JOB_ID: wiggle.D8.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D8.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D8.reverse_strandspec.2a3e0b3b02f657937db27238d589e39a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D8.reverse_strandspec.2a3e0b3b02f657937db27238d589e39a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/D8 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/D8/D8.sorted.mdup.bam \
  > alignment/D8/D8.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/D8/D8.sorted.mdup.bam \
  > alignment/D8/D8.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/D8/D8.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/D8/D8.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/D8/D8.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/D8/D8.sorted.mdup.tmp1.reverse.bam alignment/D8/D8.sorted.mdup.tmp2.reverse.bam
wiggle.D8.reverse_strandspec.2a3e0b3b02f657937db27238d589e39a.mugqic.done
)
wiggle_58_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_58_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_59_JOB_ID: wiggle.D8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D8.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D8.forward.041bde9993a119a273e652dc2603d4da.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D8.forward.041bde9993a119a273e652dc2603d4da.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D8/D8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D8/D8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D8/D8.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/D8/D8.forward.bedGraph > tracks/D8/D8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D8/D8.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D8.forward.bw
wiggle.D8.forward.041bde9993a119a273e652dc2603d4da.mugqic.done
)
wiggle_59_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_59_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_60_JOB_ID: wiggle.D8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.D8.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.D8.reverse.af6201e0d5c008b33576696d9c6c8885.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.D8.reverse.af6201e0d5c008b33576696d9c6c8885.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/D8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/D8/D8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/D8/D8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/D8/D8.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/D8/D8.reverse.bedGraph > tracks/D8/D8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/D8/D8.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/D8.reverse.bw
wiggle.D8.reverse.af6201e0d5c008b33576696d9c6c8885.mugqic.done
)
wiggle_60_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_60_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_61_JOB_ID: wiggle.E3.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E3.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E3.forward_strandspec.78ba2540f3ea2e91aa4214d681445728.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E3.forward_strandspec.78ba2540f3ea2e91aa4214d681445728.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/E3/E3.sorted.mdup.bam \
  > alignment/E3/E3.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/E3/E3.sorted.mdup.bam \
  > alignment/E3/E3.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E3/E3.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/E3/E3.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/E3/E3.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E3/E3.sorted.mdup.tmp1.forward.bam alignment/E3/E3.sorted.mdup.tmp2.forward.bam
wiggle.E3.forward_strandspec.78ba2540f3ea2e91aa4214d681445728.mugqic.done
)
wiggle_61_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_61_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_62_JOB_ID: wiggle.E3.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E3.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E3.reverse_strandspec.203ea9222a1afdf94b58466e36c474ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E3.reverse_strandspec.203ea9222a1afdf94b58466e36c474ec.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/E3 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/E3/E3.sorted.mdup.bam \
  > alignment/E3/E3.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/E3/E3.sorted.mdup.bam \
  > alignment/E3/E3.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E3/E3.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/E3/E3.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/E3/E3.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E3/E3.sorted.mdup.tmp1.reverse.bam alignment/E3/E3.sorted.mdup.tmp2.reverse.bam
wiggle.E3.reverse_strandspec.203ea9222a1afdf94b58466e36c474ec.mugqic.done
)
wiggle_62_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_62_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_63_JOB_ID: wiggle.E3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E3.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E3.forward.d9028c7bbe2f14f74362bb98200bd0fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E3.forward.d9028c7bbe2f14f74362bb98200bd0fe.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E3 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E3/E3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E3/E3.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E3/E3.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/E3/E3.forward.bedGraph > tracks/E3/E3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E3/E3.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E3.forward.bw
wiggle.E3.forward.d9028c7bbe2f14f74362bb98200bd0fe.mugqic.done
)
wiggle_63_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_63_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_64_JOB_ID: wiggle.E3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E3.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E3.reverse.237dd792fc6e4957322b39e4e6f52818.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E3.reverse.237dd792fc6e4957322b39e4e6f52818.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E3 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E3/E3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E3/E3.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E3/E3.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/E3/E3.reverse.bedGraph > tracks/E3/E3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E3/E3.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E3.reverse.bw
wiggle.E3.reverse.237dd792fc6e4957322b39e4e6f52818.mugqic.done
)
wiggle_64_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_64_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_65_JOB_ID: wiggle.E4.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E4.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E4.forward_strandspec.a922bdac27d0f8f91724ab8f7c310efb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E4.forward_strandspec.a922bdac27d0f8f91724ab8f7c310efb.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/E4/E4.sorted.mdup.bam \
  > alignment/E4/E4.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/E4/E4.sorted.mdup.bam \
  > alignment/E4/E4.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E4/E4.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/E4/E4.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/E4/E4.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E4/E4.sorted.mdup.tmp1.forward.bam alignment/E4/E4.sorted.mdup.tmp2.forward.bam
wiggle.E4.forward_strandspec.a922bdac27d0f8f91724ab8f7c310efb.mugqic.done
)
wiggle_65_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_65_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_66_JOB_ID: wiggle.E4.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E4.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E4.reverse_strandspec.22c453e55cd12a23b22368ce71975d2e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E4.reverse_strandspec.22c453e55cd12a23b22368ce71975d2e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/E4 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/E4/E4.sorted.mdup.bam \
  > alignment/E4/E4.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/E4/E4.sorted.mdup.bam \
  > alignment/E4/E4.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E4/E4.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/E4/E4.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/E4/E4.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E4/E4.sorted.mdup.tmp1.reverse.bam alignment/E4/E4.sorted.mdup.tmp2.reverse.bam
wiggle.E4.reverse_strandspec.22c453e55cd12a23b22368ce71975d2e.mugqic.done
)
wiggle_66_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_66_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_67_JOB_ID: wiggle.E4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E4.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E4.forward.2dc77eafdeb8602a59d830de915253cf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E4.forward.2dc77eafdeb8602a59d830de915253cf.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E4/E4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E4/E4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E4/E4.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/E4/E4.forward.bedGraph > tracks/E4/E4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E4/E4.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E4.forward.bw
wiggle.E4.forward.2dc77eafdeb8602a59d830de915253cf.mugqic.done
)
wiggle_67_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_67_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_68_JOB_ID: wiggle.E4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E4.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E4.reverse.b9bf300f1a9c73d876ddc05b90234cc6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E4.reverse.b9bf300f1a9c73d876ddc05b90234cc6.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E4 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E4/E4.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E4/E4.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E4/E4.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/E4/E4.reverse.bedGraph > tracks/E4/E4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E4/E4.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E4.reverse.bw
wiggle.E4.reverse.b9bf300f1a9c73d876ddc05b90234cc6.mugqic.done
)
wiggle_68_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_68_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_69_JOB_ID: wiggle.E6.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E6.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E6.forward_strandspec.3690d1f3af8862ba661a9664d6706a61.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E6.forward_strandspec.3690d1f3af8862ba661a9664d6706a61.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/E6/E6.sorted.mdup.bam \
  > alignment/E6/E6.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/E6/E6.sorted.mdup.bam \
  > alignment/E6/E6.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E6/E6.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/E6/E6.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/E6/E6.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E6/E6.sorted.mdup.tmp1.forward.bam alignment/E6/E6.sorted.mdup.tmp2.forward.bam
wiggle.E6.forward_strandspec.3690d1f3af8862ba661a9664d6706a61.mugqic.done
)
wiggle_69_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_69_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_70_JOB_ID: wiggle.E6.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E6.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E6.reverse_strandspec.3ae9c00b9a1a4214199055b17119c201.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E6.reverse_strandspec.3ae9c00b9a1a4214199055b17119c201.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/E6 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/E6/E6.sorted.mdup.bam \
  > alignment/E6/E6.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/E6/E6.sorted.mdup.bam \
  > alignment/E6/E6.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E6/E6.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/E6/E6.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/E6/E6.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E6/E6.sorted.mdup.tmp1.reverse.bam alignment/E6/E6.sorted.mdup.tmp2.reverse.bam
wiggle.E6.reverse_strandspec.3ae9c00b9a1a4214199055b17119c201.mugqic.done
)
wiggle_70_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_70_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_71_JOB_ID: wiggle.E6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E6.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E6.forward.474d58b6fa7d227e936374a8a7108c89.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E6.forward.474d58b6fa7d227e936374a8a7108c89.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E6 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E6/E6.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E6/E6.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E6/E6.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/E6/E6.forward.bedGraph > tracks/E6/E6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E6/E6.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E6.forward.bw
wiggle.E6.forward.474d58b6fa7d227e936374a8a7108c89.mugqic.done
)
wiggle_71_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_71_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_72_JOB_ID: wiggle.E6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E6.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E6.reverse.0d026df1f923e06ae7d48356ed868855.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E6.reverse.0d026df1f923e06ae7d48356ed868855.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E6 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E6/E6.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E6/E6.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E6/E6.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/E6/E6.reverse.bedGraph > tracks/E6/E6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E6/E6.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E6.reverse.bw
wiggle.E6.reverse.0d026df1f923e06ae7d48356ed868855.mugqic.done
)
wiggle_72_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_72_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_73_JOB_ID: wiggle.E8.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E8.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E8.forward_strandspec.121d1ad393b78182970ba5b8dc8ef08e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E8.forward_strandspec.121d1ad393b78182970ba5b8dc8ef08e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/E8/E8.sorted.mdup.bam \
  > alignment/E8/E8.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/E8/E8.sorted.mdup.bam \
  > alignment/E8/E8.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E8/E8.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/E8/E8.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/E8/E8.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E8/E8.sorted.mdup.tmp1.forward.bam alignment/E8/E8.sorted.mdup.tmp2.forward.bam
wiggle.E8.forward_strandspec.121d1ad393b78182970ba5b8dc8ef08e.mugqic.done
)
wiggle_73_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_73_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_74_JOB_ID: wiggle.E8.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E8.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E8.reverse_strandspec.e1642fe08e758b89b6b93b6229a5f05a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E8.reverse_strandspec.e1642fe08e758b89b6b93b6229a5f05a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/E8 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/E8/E8.sorted.mdup.bam \
  > alignment/E8/E8.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/E8/E8.sorted.mdup.bam \
  > alignment/E8/E8.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E8/E8.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/E8/E8.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/E8/E8.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E8/E8.sorted.mdup.tmp1.reverse.bam alignment/E8/E8.sorted.mdup.tmp2.reverse.bam
wiggle.E8.reverse_strandspec.e1642fe08e758b89b6b93b6229a5f05a.mugqic.done
)
wiggle_74_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_74_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_75_JOB_ID: wiggle.E8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E8.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E8.forward.e23b5229d081b445e43fc6267b6af46f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E8.forward.e23b5229d081b445e43fc6267b6af46f.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E8/E8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E8/E8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E8/E8.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/E8/E8.forward.bedGraph > tracks/E8/E8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E8/E8.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E8.forward.bw
wiggle.E8.forward.e23b5229d081b445e43fc6267b6af46f.mugqic.done
)
wiggle_75_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_75_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_76_JOB_ID: wiggle.E8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E8.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_19_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E8.reverse.5b8ca9382bbb844254137fadfec98a88.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E8.reverse.5b8ca9382bbb844254137fadfec98a88.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E8 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E8/E8.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E8/E8.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E8/E8.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/E8/E8.reverse.bedGraph > tracks/E8/E8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E8/E8.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E8.reverse.bw
wiggle.E8.reverse.5b8ca9382bbb844254137fadfec98a88.mugqic.done
)
wiggle_76_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_76_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_77_JOB_ID: wiggle.E9.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E9.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E9.forward_strandspec.11427d28f177e91ab1a818d19dbb06c4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E9.forward_strandspec.11427d28f177e91ab1a818d19dbb06c4.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
samtools view -bh -F 256 -f 81 \
  alignment/E9/E9.sorted.mdup.bam \
  > alignment/E9/E9.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/E9/E9.sorted.mdup.bam \
  > alignment/E9/E9.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E9/E9.sorted.mdup.tmp1.forward.bam \
  INPUT=alignment/E9/E9.sorted.mdup.tmp2.forward.bam \
  OUTPUT=alignment/E9/E9.sorted.mdup.forward.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E9/E9.sorted.mdup.tmp1.forward.bam alignment/E9/E9.sorted.mdup.tmp2.forward.bam
wiggle.E9.forward_strandspec.11427d28f177e91ab1a818d19dbb06c4.mugqic.done
)
wiggle_77_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_77_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_78_JOB_ID: wiggle.E9.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E9.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E9.reverse_strandspec.9386e54172374c1503b6a50a70a355d3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E9.reverse_strandspec.9386e54172374c1503b6a50a70a355d3.mugqic.done'
module load mugqic/samtools/1.3 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p tracks/E9 tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/E9/E9.sorted.mdup.bam \
  > alignment/E9/E9.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/E9/E9.sorted.mdup.bam \
  > alignment/E9/E9.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $PICARD_HOME/MergeSamFiles.jar \
  VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=alignment/E9/E9.sorted.mdup.tmp1.reverse.bam \
  INPUT=alignment/E9/E9.sorted.mdup.tmp2.reverse.bam \
  OUTPUT=alignment/E9/E9.sorted.mdup.reverse.bam \
  MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/E9/E9.sorted.mdup.tmp1.reverse.bam alignment/E9/E9.sorted.mdup.tmp2.reverse.bam
wiggle.E9.reverse_strandspec.9386e54172374c1503b6a50a70a355d3.mugqic.done
)
wiggle_78_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_78_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_79_JOB_ID: wiggle.E9.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E9.forward
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E9.forward.a7f598bb79276f2fc8c23af59341276d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E9.forward.a7f598bb79276f2fc8c23af59341276d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E9 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E9/E9.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E9/E9.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E9/E9.forward.bedGraph && \
sort -k1,1 -k2,2n tracks/E9/E9.forward.bedGraph > tracks/E9/E9.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E9/E9.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E9.forward.bw
wiggle.E9.forward.a7f598bb79276f2fc8c23af59341276d.mugqic.done
)
wiggle_79_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_79_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_80_JOB_ID: wiggle.E9.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.E9.reverse
JOB_DEPENDENCIES=$picard_mark_duplicates_20_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.E9.reverse.825fdbc1f033972451b2a02a659baa7a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.E9.reverse.825fdbc1f033972451b2a02a659baa7a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/bedtools/2.25.0 mugqic/ucsc/v326 && \
mkdir -p tracks/E9 tracks/bigWig && \
nmblines=$(samtools view -F 256 -f 81  alignment/E9/E9.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed -bg -split -scale $scalefactor \
  -ibam alignment/E9/E9.sorted.mdup.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  > tracks/E9/E9.reverse.bedGraph && \
sort -k1,1 -k2,2n tracks/E9/E9.reverse.bedGraph > tracks/E9/E9.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/E9/E9.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai \
  tracks/bigWig/E9.reverse.bw
wiggle.E9.reverse.825fdbc1f033972451b2a02a659baa7a.mugqic.done
)
wiggle_80_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=12:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$wiggle_80_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: raw_counts
#-------------------------------------------------------------------------------
STEP=raw_counts
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_1_JOB_ID: htseq_count.B11
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.B11
JOB_DEPENDENCIES=$picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.B11.2705fdf7dea19766fc502845954b137d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.B11.2705fdf7dea19766fc502845954b137d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/B11/B11.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/B11.readcounts.csv
htseq_count.B11.2705fdf7dea19766fc502845954b137d.mugqic.done
)
raw_counts_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_2_JOB_ID: htseq_count.B12
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.B12
JOB_DEPENDENCIES=$picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.B12.e344f06865e8ec753eb83ae4cd30ed3e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.B12.e344f06865e8ec753eb83ae4cd30ed3e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/B12/B12.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/B12.readcounts.csv
htseq_count.B12.e344f06865e8ec753eb83ae4cd30ed3e.mugqic.done
)
raw_counts_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_3_JOB_ID: htseq_count.B1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.B1
JOB_DEPENDENCIES=$picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.B1.da939f86f52b480293d2c84295fae4bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.B1.da939f86f52b480293d2c84295fae4bd.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/B1/B1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/B1.readcounts.csv
htseq_count.B1.da939f86f52b480293d2c84295fae4bd.mugqic.done
)
raw_counts_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_4_JOB_ID: htseq_count.B6
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.B6
JOB_DEPENDENCIES=$picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.B6.07f497522267bdc5b0cc01b4061a0d46.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.B6.07f497522267bdc5b0cc01b4061a0d46.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/B6/B6.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/B6.readcounts.csv
htseq_count.B6.07f497522267bdc5b0cc01b4061a0d46.mugqic.done
)
raw_counts_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_5_JOB_ID: htseq_count.B8
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.B8
JOB_DEPENDENCIES=$picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.B8.dfbae24b661a7299a00e7296cf8c7c54.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.B8.dfbae24b661a7299a00e7296cf8c7c54.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/B8/B8.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/B8.readcounts.csv
htseq_count.B8.dfbae24b661a7299a00e7296cf8c7c54.mugqic.done
)
raw_counts_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_6_JOB_ID: htseq_count.C11
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.C11
JOB_DEPENDENCIES=$picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.C11.68d369b5e794f54260e78c36a70ea335.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.C11.68d369b5e794f54260e78c36a70ea335.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/C11/C11.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/C11.readcounts.csv
htseq_count.C11.68d369b5e794f54260e78c36a70ea335.mugqic.done
)
raw_counts_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_7_JOB_ID: htseq_count.C12
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.C12
JOB_DEPENDENCIES=$picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.C12.793571d2cbc2f3a903a9d0c8d42ba62e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.C12.793571d2cbc2f3a903a9d0c8d42ba62e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/C12/C12.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/C12.readcounts.csv
htseq_count.C12.793571d2cbc2f3a903a9d0c8d42ba62e.mugqic.done
)
raw_counts_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_8_JOB_ID: htseq_count.C4
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.C4
JOB_DEPENDENCIES=$picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.C4.9d0dd5be8f828367f5ef13d455de9822.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.C4.9d0dd5be8f828367f5ef13d455de9822.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/C4/C4.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/C4.readcounts.csv
htseq_count.C4.9d0dd5be8f828367f5ef13d455de9822.mugqic.done
)
raw_counts_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_9_JOB_ID: htseq_count.C5
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.C5
JOB_DEPENDENCIES=$picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.C5.289bf7e2560f84d92a7e4f9e98331079.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.C5.289bf7e2560f84d92a7e4f9e98331079.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/C5/C5.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/C5.readcounts.csv
htseq_count.C5.289bf7e2560f84d92a7e4f9e98331079.mugqic.done
)
raw_counts_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_10_JOB_ID: htseq_count.C9
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.C9
JOB_DEPENDENCIES=$picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.C9.025db264e755a38960ee2118e78af28c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.C9.025db264e755a38960ee2118e78af28c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/C9/C9.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/C9.readcounts.csv
htseq_count.C9.025db264e755a38960ee2118e78af28c.mugqic.done
)
raw_counts_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_11_JOB_ID: htseq_count.D12
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.D12
JOB_DEPENDENCIES=$picard_sort_sam_11_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.D12.98c07319cc48f79a72665501925a9ba6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.D12.98c07319cc48f79a72665501925a9ba6.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/D12/D12.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/D12.readcounts.csv
htseq_count.D12.98c07319cc48f79a72665501925a9ba6.mugqic.done
)
raw_counts_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_12_JOB_ID: htseq_count.D2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.D2
JOB_DEPENDENCIES=$picard_sort_sam_12_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.D2.1c9c0b37f9983dedbc9c12c7ca5f043b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.D2.1c9c0b37f9983dedbc9c12c7ca5f043b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/D2/D2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/D2.readcounts.csv
htseq_count.D2.1c9c0b37f9983dedbc9c12c7ca5f043b.mugqic.done
)
raw_counts_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_13_JOB_ID: htseq_count.D3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.D3
JOB_DEPENDENCIES=$picard_sort_sam_13_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.D3.ef84017e169961f3e2311e342c96564b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.D3.ef84017e169961f3e2311e342c96564b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/D3/D3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/D3.readcounts.csv
htseq_count.D3.ef84017e169961f3e2311e342c96564b.mugqic.done
)
raw_counts_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_14_JOB_ID: htseq_count.D4
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.D4
JOB_DEPENDENCIES=$picard_sort_sam_14_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.D4.fb7fc04c0b1b3399cc0235fdf3f1b1a1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.D4.fb7fc04c0b1b3399cc0235fdf3f1b1a1.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/D4/D4.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/D4.readcounts.csv
htseq_count.D4.fb7fc04c0b1b3399cc0235fdf3f1b1a1.mugqic.done
)
raw_counts_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_15_JOB_ID: htseq_count.D8
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.D8
JOB_DEPENDENCIES=$picard_sort_sam_15_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.D8.b81784641dc10a5b467fdc5023441137.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.D8.b81784641dc10a5b467fdc5023441137.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/D8/D8.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/D8.readcounts.csv
htseq_count.D8.b81784641dc10a5b467fdc5023441137.mugqic.done
)
raw_counts_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_16_JOB_ID: htseq_count.E3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.E3
JOB_DEPENDENCIES=$picard_sort_sam_16_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.E3.db28139d03499865e49afc7788c7cf17.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.E3.db28139d03499865e49afc7788c7cf17.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/E3/E3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/E3.readcounts.csv
htseq_count.E3.db28139d03499865e49afc7788c7cf17.mugqic.done
)
raw_counts_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_17_JOB_ID: htseq_count.E4
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.E4
JOB_DEPENDENCIES=$picard_sort_sam_17_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.E4.7249ea9530967fe2cd92c9592c98a0c2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.E4.7249ea9530967fe2cd92c9592c98a0c2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/E4/E4.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/E4.readcounts.csv
htseq_count.E4.7249ea9530967fe2cd92c9592c98a0c2.mugqic.done
)
raw_counts_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_18_JOB_ID: htseq_count.E6
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.E6
JOB_DEPENDENCIES=$picard_sort_sam_18_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.E6.00243b5af02212298594ffd9897df078.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.E6.00243b5af02212298594ffd9897df078.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/E6/E6.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/E6.readcounts.csv
htseq_count.E6.00243b5af02212298594ffd9897df078.mugqic.done
)
raw_counts_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_19_JOB_ID: htseq_count.E8
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.E8
JOB_DEPENDENCIES=$picard_sort_sam_19_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.E8.d808265aea3b53073fc7b1b10dab1cc4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.E8.d808265aea3b53073fc7b1b10dab1cc4.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/E8/E8.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/E8.readcounts.csv
htseq_count.E8.d808265aea3b53073fc7b1b10dab1cc4.mugqic.done
)
raw_counts_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_20_JOB_ID: htseq_count.E9
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.E9
JOB_DEPENDENCIES=$picard_sort_sam_20_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.E9.1c8140be4ab9724fd4cb64b981fa95bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.E9.1c8140be4ab9724fd4cb64b981fa95bd.mugqic.done'
module load mugqic/samtools/1.3 mugqic/python/2.7.11 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/E9/E9.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  > raw_counts/E9.readcounts.csv
htseq_count.E9.1c8140be4ab9724fd4cb64b981fa95bd.mugqic.done
)
raw_counts_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: raw_counts_metrics
#-------------------------------------------------------------------------------
STEP=raw_counts_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_1_JOB_ID: metrics.matrix
#-------------------------------------------------------------------------------
JOB_NAME=metrics.matrix
JOB_DEPENDENCIES=$raw_counts_1_JOB_ID:$raw_counts_2_JOB_ID:$raw_counts_3_JOB_ID:$raw_counts_4_JOB_ID:$raw_counts_5_JOB_ID:$raw_counts_6_JOB_ID:$raw_counts_7_JOB_ID:$raw_counts_8_JOB_ID:$raw_counts_9_JOB_ID:$raw_counts_10_JOB_ID:$raw_counts_11_JOB_ID:$raw_counts_12_JOB_ID:$raw_counts_13_JOB_ID:$raw_counts_14_JOB_ID:$raw_counts_15_JOB_ID:$raw_counts_16_JOB_ID:$raw_counts_17_JOB_ID:$raw_counts_18_JOB_ID:$raw_counts_19_JOB_ID:$raw_counts_20_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.matrix.4581399d3907b87c38358dcbba112441.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.matrix.4581399d3907b87c38358dcbba112441.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 && \
mkdir -p DGE && \
gtf2tmpMatrix.awk \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  DGE/tmpMatrix.txt && \
HEAD='Gene\tSymbol' && \
for read_count_file in \
  raw_counts/B11.readcounts.csv \
  raw_counts/B12.readcounts.csv \
  raw_counts/B1.readcounts.csv \
  raw_counts/B6.readcounts.csv \
  raw_counts/B8.readcounts.csv \
  raw_counts/C11.readcounts.csv \
  raw_counts/C12.readcounts.csv \
  raw_counts/C4.readcounts.csv \
  raw_counts/C5.readcounts.csv \
  raw_counts/C9.readcounts.csv \
  raw_counts/D12.readcounts.csv \
  raw_counts/D2.readcounts.csv \
  raw_counts/D3.readcounts.csv \
  raw_counts/D4.readcounts.csv \
  raw_counts/D8.readcounts.csv \
  raw_counts/E3.readcounts.csv \
  raw_counts/E4.readcounts.csv \
  raw_counts/E6.readcounts.csv \
  raw_counts/E8.readcounts.csv \
  raw_counts/E9.readcounts.csv
do
  sort -k1,1 $read_count_file > DGE/tmpSort.txt && \
  join -1 1 -2 1 <(sort -k1,1 DGE/tmpMatrix.txt) DGE/tmpSort.txt > DGE/tmpMatrix.2.txt && \
  mv DGE/tmpMatrix.2.txt DGE/tmpMatrix.txt && \
  na=$(basename $read_count_file | cut -d. -f1) && \
  HEAD="$HEAD\t$na"
done && \
echo -e $HEAD | cat - DGE/tmpMatrix.txt | tr ' ' '\t' > DGE/rawCountMatrix.csv && \
rm DGE/tmpSort.txt DGE/tmpMatrix.txt
metrics.matrix.4581399d3907b87c38358dcbba112441.mugqic.done
)
raw_counts_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_2_JOB_ID: metrics.wigzip
#-------------------------------------------------------------------------------
JOB_NAME=metrics.wigzip
JOB_DEPENDENCIES=$wiggle_3_JOB_ID:$wiggle_4_JOB_ID:$wiggle_7_JOB_ID:$wiggle_8_JOB_ID:$wiggle_11_JOB_ID:$wiggle_12_JOB_ID:$wiggle_15_JOB_ID:$wiggle_16_JOB_ID:$wiggle_19_JOB_ID:$wiggle_20_JOB_ID:$wiggle_23_JOB_ID:$wiggle_24_JOB_ID:$wiggle_27_JOB_ID:$wiggle_28_JOB_ID:$wiggle_31_JOB_ID:$wiggle_32_JOB_ID:$wiggle_35_JOB_ID:$wiggle_36_JOB_ID:$wiggle_39_JOB_ID:$wiggle_40_JOB_ID:$wiggle_43_JOB_ID:$wiggle_44_JOB_ID:$wiggle_47_JOB_ID:$wiggle_48_JOB_ID:$wiggle_51_JOB_ID:$wiggle_52_JOB_ID:$wiggle_55_JOB_ID:$wiggle_56_JOB_ID:$wiggle_59_JOB_ID:$wiggle_60_JOB_ID:$wiggle_63_JOB_ID:$wiggle_64_JOB_ID:$wiggle_67_JOB_ID:$wiggle_68_JOB_ID:$wiggle_71_JOB_ID:$wiggle_72_JOB_ID:$wiggle_75_JOB_ID:$wiggle_76_JOB_ID:$wiggle_79_JOB_ID:$wiggle_80_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done'
zip -r tracks.zip tracks/bigWig
metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
)
raw_counts_metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=5:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_3_JOB_ID: rpkm_saturation
#-------------------------------------------------------------------------------
JOB_NAME=rpkm_saturation
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/rpkm_saturation.73b1004c81c4ca09a37b813f110ac931.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rpkm_saturation.73b1004c81c4ca09a37b813f110ac931.mugqic.done'
module load mugqic/R_Bioconductor/3.2.3_3.2 mugqic/mugqic_tools/2.1.5 && \
mkdir -p metrics/saturation && \
Rscript $R_TOOLS/rpkmSaturation.R \
  DGE/rawCountMatrix.csv \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.genes.length.tsv \
  raw_counts \
  metrics/saturation \
  11 \
  1 && \
zip -r metrics/saturation.zip metrics/saturation
rpkm_saturation.73b1004c81c4ca09a37b813f110ac931.mugqic.done
)
raw_counts_metrics_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q lm -l nodes=1:ppn=12 -l pmem=5700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_4_JOB_ID: raw_count_metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=raw_count_metrics_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID:$raw_counts_metrics_2_JOB_ID:$raw_counts_metrics_3_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/raw_count_metrics_report.860f7f2b97a9a18b95b0e754b87a26ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'raw_count_metrics_report.860f7f2b97a9a18b95b0e754b87a26ec.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep/corrMatrixSpearman.txt report/corrMatrixSpearman.tsv && \
cp tracks.zip report/ && \
cp metrics/saturation.zip report/ && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.raw_counts_metrics.md \
  --variable corr_matrix_spearman_table="`head -16 report/corrMatrixSpearman.tsv | cut -f-16| awk -F"	" '{OFS="	"; if (NR==1) {$0="Vs"$0; print; gsub(/[^	]/, "-"); print} else {printf $1; for (i=2; i<=NF; i++) {printf "	"sprintf("%.2f", $i)}; print ""}}' | sed 's/	/|/g'`" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.raw_counts_metrics.md \
  > report/RnaSeq.raw_counts_metrics.md
raw_count_metrics_report.860f7f2b97a9a18b95b0e754b87a26ec.mugqic.done
)
raw_counts_metrics_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$raw_counts_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cufflinks
#-------------------------------------------------------------------------------
STEP=cufflinks
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cufflinks_1_JOB_ID: cufflinks.B11
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.B11
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.B11.db64c64d233dc9b567a9fe162545b88a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.B11.db64c64d233dc9b567a9fe162545b88a.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B11 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B11 \
  --num-threads 12 \
  alignment/B11/B11.sorted.mdup.hardClip.bam
cufflinks.B11.db64c64d233dc9b567a9fe162545b88a.mugqic.done
)
cufflinks_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_2_JOB_ID: cufflinks.B12
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.B12
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.B12.831fece22a1b2dd632ade36dd16ac192.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.B12.831fece22a1b2dd632ade36dd16ac192.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B12 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B12 \
  --num-threads 12 \
  alignment/B12/B12.sorted.mdup.hardClip.bam
cufflinks.B12.831fece22a1b2dd632ade36dd16ac192.mugqic.done
)
cufflinks_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_3_JOB_ID: cufflinks.B1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.B1
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.B1.188f3a5c8e335a478f611de69fa7de7b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.B1.188f3a5c8e335a478f611de69fa7de7b.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B1 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B1 \
  --num-threads 12 \
  alignment/B1/B1.sorted.mdup.hardClip.bam
cufflinks.B1.188f3a5c8e335a478f611de69fa7de7b.mugqic.done
)
cufflinks_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_4_JOB_ID: cufflinks.B6
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.B6
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.B6.92f20513896ad98349c10824e56d78d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.B6.92f20513896ad98349c10824e56d78d1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B6 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B6 \
  --num-threads 12 \
  alignment/B6/B6.sorted.mdup.hardClip.bam
cufflinks.B6.92f20513896ad98349c10824e56d78d1.mugqic.done
)
cufflinks_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_5_JOB_ID: cufflinks.B8
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.B8
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.B8.0804cf9e8673a99d70a32cf5e8d239ab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.B8.0804cf9e8673a99d70a32cf5e8d239ab.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B8 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B8 \
  --num-threads 12 \
  alignment/B8/B8.sorted.mdup.hardClip.bam
cufflinks.B8.0804cf9e8673a99d70a32cf5e8d239ab.mugqic.done
)
cufflinks_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_6_JOB_ID: cufflinks.C11
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.C11
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.C11.9987ef8d5032d66744d076ec2eb3a2e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.C11.9987ef8d5032d66744d076ec2eb3a2e8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C11 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C11 \
  --num-threads 12 \
  alignment/C11/C11.sorted.mdup.hardClip.bam
cufflinks.C11.9987ef8d5032d66744d076ec2eb3a2e8.mugqic.done
)
cufflinks_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_7_JOB_ID: cufflinks.C12
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.C12
JOB_DEPENDENCIES=$bam_hard_clip_7_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.C12.e11401446781f665ca341779ff5796c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.C12.e11401446781f665ca341779ff5796c6.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C12 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C12 \
  --num-threads 12 \
  alignment/C12/C12.sorted.mdup.hardClip.bam
cufflinks.C12.e11401446781f665ca341779ff5796c6.mugqic.done
)
cufflinks_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_8_JOB_ID: cufflinks.C4
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.C4
JOB_DEPENDENCIES=$bam_hard_clip_8_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.C4.1cb90518f851e430794a86dfd34c3df0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.C4.1cb90518f851e430794a86dfd34c3df0.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C4 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C4 \
  --num-threads 12 \
  alignment/C4/C4.sorted.mdup.hardClip.bam
cufflinks.C4.1cb90518f851e430794a86dfd34c3df0.mugqic.done
)
cufflinks_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_9_JOB_ID: cufflinks.C5
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.C5
JOB_DEPENDENCIES=$bam_hard_clip_9_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.C5.183de985e085811acaeaed0aeb972047.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.C5.183de985e085811acaeaed0aeb972047.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C5 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C5 \
  --num-threads 12 \
  alignment/C5/C5.sorted.mdup.hardClip.bam
cufflinks.C5.183de985e085811acaeaed0aeb972047.mugqic.done
)
cufflinks_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_10_JOB_ID: cufflinks.C9
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.C9
JOB_DEPENDENCIES=$bam_hard_clip_10_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.C9.6d30bc68ba0b3289399f5a695afb8151.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.C9.6d30bc68ba0b3289399f5a695afb8151.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C9 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C9 \
  --num-threads 12 \
  alignment/C9/C9.sorted.mdup.hardClip.bam
cufflinks.C9.6d30bc68ba0b3289399f5a695afb8151.mugqic.done
)
cufflinks_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_11_JOB_ID: cufflinks.D12
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.D12
JOB_DEPENDENCIES=$bam_hard_clip_11_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.D12.be8399e5f2f1c9546120c6ff77b37699.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.D12.be8399e5f2f1c9546120c6ff77b37699.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D12 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D12 \
  --num-threads 12 \
  alignment/D12/D12.sorted.mdup.hardClip.bam
cufflinks.D12.be8399e5f2f1c9546120c6ff77b37699.mugqic.done
)
cufflinks_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_12_JOB_ID: cufflinks.D2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.D2
JOB_DEPENDENCIES=$bam_hard_clip_12_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.D2.c42b4e4aa99fe1f86d3690daa5ecae16.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.D2.c42b4e4aa99fe1f86d3690daa5ecae16.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D2 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D2 \
  --num-threads 12 \
  alignment/D2/D2.sorted.mdup.hardClip.bam
cufflinks.D2.c42b4e4aa99fe1f86d3690daa5ecae16.mugqic.done
)
cufflinks_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_13_JOB_ID: cufflinks.D3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.D3
JOB_DEPENDENCIES=$bam_hard_clip_13_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.D3.35fd4deac72c444c23b507eb24bdf82f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.D3.35fd4deac72c444c23b507eb24bdf82f.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D3 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D3 \
  --num-threads 12 \
  alignment/D3/D3.sorted.mdup.hardClip.bam
cufflinks.D3.35fd4deac72c444c23b507eb24bdf82f.mugqic.done
)
cufflinks_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_14_JOB_ID: cufflinks.D4
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.D4
JOB_DEPENDENCIES=$bam_hard_clip_14_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.D4.0428d8e0072561744a437eab02c230e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.D4.0428d8e0072561744a437eab02c230e8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D4 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D4 \
  --num-threads 12 \
  alignment/D4/D4.sorted.mdup.hardClip.bam
cufflinks.D4.0428d8e0072561744a437eab02c230e8.mugqic.done
)
cufflinks_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_15_JOB_ID: cufflinks.D8
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.D8
JOB_DEPENDENCIES=$bam_hard_clip_15_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.D8.d60ecc35fadee2394ea8002b901b66e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.D8.d60ecc35fadee2394ea8002b901b66e8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D8 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D8 \
  --num-threads 12 \
  alignment/D8/D8.sorted.mdup.hardClip.bam
cufflinks.D8.d60ecc35fadee2394ea8002b901b66e8.mugqic.done
)
cufflinks_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_16_JOB_ID: cufflinks.E3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.E3
JOB_DEPENDENCIES=$bam_hard_clip_16_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.E3.e2496988e8663f61636c7f80117125b7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.E3.e2496988e8663f61636c7f80117125b7.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E3 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E3 \
  --num-threads 12 \
  alignment/E3/E3.sorted.mdup.hardClip.bam
cufflinks.E3.e2496988e8663f61636c7f80117125b7.mugqic.done
)
cufflinks_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_17_JOB_ID: cufflinks.E4
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.E4
JOB_DEPENDENCIES=$bam_hard_clip_17_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.E4.bf0c391b2b87610b4aa2fb574d303dc5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.E4.bf0c391b2b87610b4aa2fb574d303dc5.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E4 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E4 \
  --num-threads 12 \
  alignment/E4/E4.sorted.mdup.hardClip.bam
cufflinks.E4.bf0c391b2b87610b4aa2fb574d303dc5.mugqic.done
)
cufflinks_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_18_JOB_ID: cufflinks.E6
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.E6
JOB_DEPENDENCIES=$bam_hard_clip_18_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.E6.76adec46f2217f39862619e9144651bb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.E6.76adec46f2217f39862619e9144651bb.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E6 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E6 \
  --num-threads 12 \
  alignment/E6/E6.sorted.mdup.hardClip.bam
cufflinks.E6.76adec46f2217f39862619e9144651bb.mugqic.done
)
cufflinks_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_19_JOB_ID: cufflinks.E8
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.E8
JOB_DEPENDENCIES=$bam_hard_clip_19_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.E8.ca1c7d85d8d4f3ebfaa80c0191c73fb1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.E8.ca1c7d85d8d4f3ebfaa80c0191c73fb1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E8 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E8 \
  --num-threads 12 \
  alignment/E8/E8.sorted.mdup.hardClip.bam
cufflinks.E8.ca1c7d85d8d4f3ebfaa80c0191c73fb1.mugqic.done
)
cufflinks_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_20_JOB_ID: cufflinks.E9
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.E9
JOB_DEPENDENCIES=$bam_hard_clip_20_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.E9.2f1ef25c67b2cb5c0f3f4cd18bd48efa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.E9.2f1ef25c67b2cb5c0f3f4cd18bd48efa.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E9 && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E9 \
  --num-threads 12 \
  alignment/E9/E9.sorted.mdup.hardClip.bam
cufflinks.E9.2f1ef25c67b2cb5c0f3f4cd18bd48efa.mugqic.done
)
cufflinks_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cufflinks_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffmerge
#-------------------------------------------------------------------------------
STEP=cuffmerge
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffmerge_1_JOB_ID: cuffmerge
#-------------------------------------------------------------------------------
JOB_NAME=cuffmerge
JOB_DEPENDENCIES=$cufflinks_1_JOB_ID:$cufflinks_2_JOB_ID:$cufflinks_3_JOB_ID:$cufflinks_4_JOB_ID:$cufflinks_5_JOB_ID:$cufflinks_6_JOB_ID:$cufflinks_7_JOB_ID:$cufflinks_8_JOB_ID:$cufflinks_9_JOB_ID:$cufflinks_10_JOB_ID:$cufflinks_11_JOB_ID:$cufflinks_12_JOB_ID:$cufflinks_13_JOB_ID:$cufflinks_14_JOB_ID:$cufflinks_15_JOB_ID:$cufflinks_16_JOB_ID:$cufflinks_17_JOB_ID:$cufflinks_18_JOB_ID:$cufflinks_19_JOB_ID:$cufflinks_20_JOB_ID
JOB_DONE=job_output/cuffmerge/cuffmerge.ff80a0c87417df98f4e8688d801ba6c9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffmerge.ff80a0c87417df98f4e8688d801ba6c9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/AllSamples && \
`cat > cufflinks/cuffmerge.samples.txt << END
cufflinks/B11/transcripts.gtf
cufflinks/B12/transcripts.gtf
cufflinks/B1/transcripts.gtf
cufflinks/B6/transcripts.gtf
cufflinks/B8/transcripts.gtf
cufflinks/C11/transcripts.gtf
cufflinks/C12/transcripts.gtf
cufflinks/C4/transcripts.gtf
cufflinks/C5/transcripts.gtf
cufflinks/C9/transcripts.gtf
cufflinks/D12/transcripts.gtf
cufflinks/D2/transcripts.gtf
cufflinks/D3/transcripts.gtf
cufflinks/D4/transcripts.gtf
cufflinks/D8/transcripts.gtf
cufflinks/E3/transcripts.gtf
cufflinks/E4/transcripts.gtf
cufflinks/E6/transcripts.gtf
cufflinks/E8/transcripts.gtf
cufflinks/E9/transcripts.gtf
END
  
` && \
cuffmerge  \
  --ref-gtf /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.gtf \
  --ref-sequence /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  -o cufflinks/AllSamples \
  --num-threads 12 \
  cufflinks/cuffmerge.samples.txt
cuffmerge.ff80a0c87417df98f4e8688d801ba6c9.mugqic.done
)
cuffmerge_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffmerge_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffquant
#-------------------------------------------------------------------------------
STEP=cuffquant
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffquant_1_JOB_ID: cuffquant.B11
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.B11
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.B11.e70df7e5b3f8562ce0c3f9620647a84b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.B11.e70df7e5b3f8562ce0c3f9620647a84b.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B11 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B11 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/B11/B11.sorted.mdup.hardClip.bam
cuffquant.B11.e70df7e5b3f8562ce0c3f9620647a84b.mugqic.done
)
cuffquant_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_2_JOB_ID: cuffquant.B12
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.B12
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.B12.203bcc73f353b41e859a318f589b3ce7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.B12.203bcc73f353b41e859a318f589b3ce7.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B12 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B12 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/B12/B12.sorted.mdup.hardClip.bam
cuffquant.B12.203bcc73f353b41e859a318f589b3ce7.mugqic.done
)
cuffquant_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_3_JOB_ID: cuffquant.B1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.B1
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.B1.395249583197410fbe23ae2c7b77d1df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.B1.395249583197410fbe23ae2c7b77d1df.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B1 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/B1/B1.sorted.mdup.hardClip.bam
cuffquant.B1.395249583197410fbe23ae2c7b77d1df.mugqic.done
)
cuffquant_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_4_JOB_ID: cuffquant.B6
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.B6
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.B6.9887999f8aa5ac68ed4affafa793e19a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.B6.9887999f8aa5ac68ed4affafa793e19a.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B6 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B6 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/B6/B6.sorted.mdup.hardClip.bam
cuffquant.B6.9887999f8aa5ac68ed4affafa793e19a.mugqic.done
)
cuffquant_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_5_JOB_ID: cuffquant.B8
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.B8
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.B8.ca5774c783bed9dcd4a7508040104a93.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.B8.ca5774c783bed9dcd4a7508040104a93.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/B8 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/B8 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/B8/B8.sorted.mdup.hardClip.bam
cuffquant.B8.ca5774c783bed9dcd4a7508040104a93.mugqic.done
)
cuffquant_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_6_JOB_ID: cuffquant.C11
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.C11
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.C11.b9a63d5f9f4339351f58d53d97b9c531.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.C11.b9a63d5f9f4339351f58d53d97b9c531.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C11 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C11 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/C11/C11.sorted.mdup.hardClip.bam
cuffquant.C11.b9a63d5f9f4339351f58d53d97b9c531.mugqic.done
)
cuffquant_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_7_JOB_ID: cuffquant.C12
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.C12
JOB_DEPENDENCIES=$bam_hard_clip_7_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.C12.ab8c22d7a479e44209ca7d87e7ff1187.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.C12.ab8c22d7a479e44209ca7d87e7ff1187.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C12 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C12 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/C12/C12.sorted.mdup.hardClip.bam
cuffquant.C12.ab8c22d7a479e44209ca7d87e7ff1187.mugqic.done
)
cuffquant_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_8_JOB_ID: cuffquant.C4
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.C4
JOB_DEPENDENCIES=$bam_hard_clip_8_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.C4.7e72b6731904c60a68ac16bdc10ebb4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.C4.7e72b6731904c60a68ac16bdc10ebb4f.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C4 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C4 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/C4/C4.sorted.mdup.hardClip.bam
cuffquant.C4.7e72b6731904c60a68ac16bdc10ebb4f.mugqic.done
)
cuffquant_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_9_JOB_ID: cuffquant.C5
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.C5
JOB_DEPENDENCIES=$bam_hard_clip_9_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.C5.582ae405a2073a429953f2598b2175d9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.C5.582ae405a2073a429953f2598b2175d9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C5 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C5 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/C5/C5.sorted.mdup.hardClip.bam
cuffquant.C5.582ae405a2073a429953f2598b2175d9.mugqic.done
)
cuffquant_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_10_JOB_ID: cuffquant.C9
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.C9
JOB_DEPENDENCIES=$bam_hard_clip_10_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.C9.161224b54587e5905e81240c5078ddf6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.C9.161224b54587e5905e81240c5078ddf6.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/C9 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/C9 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/C9/C9.sorted.mdup.hardClip.bam
cuffquant.C9.161224b54587e5905e81240c5078ddf6.mugqic.done
)
cuffquant_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_11_JOB_ID: cuffquant.D12
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.D12
JOB_DEPENDENCIES=$bam_hard_clip_11_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.D12.cc39832f96168a6753c0882b471830fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.D12.cc39832f96168a6753c0882b471830fd.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D12 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D12 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/D12/D12.sorted.mdup.hardClip.bam
cuffquant.D12.cc39832f96168a6753c0882b471830fd.mugqic.done
)
cuffquant_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_12_JOB_ID: cuffquant.D2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.D2
JOB_DEPENDENCIES=$bam_hard_clip_12_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.D2.9a4b04184c050630d80b0987893b80ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.D2.9a4b04184c050630d80b0987893b80ed.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D2 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/D2/D2.sorted.mdup.hardClip.bam
cuffquant.D2.9a4b04184c050630d80b0987893b80ed.mugqic.done
)
cuffquant_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_13_JOB_ID: cuffquant.D3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.D3
JOB_DEPENDENCIES=$bam_hard_clip_13_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.D3.a5530dec241b366b5129b118dd402974.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.D3.a5530dec241b366b5129b118dd402974.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D3 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/D3/D3.sorted.mdup.hardClip.bam
cuffquant.D3.a5530dec241b366b5129b118dd402974.mugqic.done
)
cuffquant_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_14_JOB_ID: cuffquant.D4
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.D4
JOB_DEPENDENCIES=$bam_hard_clip_14_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.D4.62dadd8c42540bf23b205b6c78d2071f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.D4.62dadd8c42540bf23b205b6c78d2071f.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D4 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D4 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/D4/D4.sorted.mdup.hardClip.bam
cuffquant.D4.62dadd8c42540bf23b205b6c78d2071f.mugqic.done
)
cuffquant_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_15_JOB_ID: cuffquant.D8
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.D8
JOB_DEPENDENCIES=$bam_hard_clip_15_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.D8.85d86c8a9e4aaef0d8a841b17f6667ee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.D8.85d86c8a9e4aaef0d8a841b17f6667ee.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/D8 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/D8 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/D8/D8.sorted.mdup.hardClip.bam
cuffquant.D8.85d86c8a9e4aaef0d8a841b17f6667ee.mugqic.done
)
cuffquant_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_16_JOB_ID: cuffquant.E3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.E3
JOB_DEPENDENCIES=$bam_hard_clip_16_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.E3.d9b3bf8b450bb47d592ed4a9704dd5e6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.E3.d9b3bf8b450bb47d592ed4a9704dd5e6.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E3 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/E3/E3.sorted.mdup.hardClip.bam
cuffquant.E3.d9b3bf8b450bb47d592ed4a9704dd5e6.mugqic.done
)
cuffquant_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_17_JOB_ID: cuffquant.E4
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.E4
JOB_DEPENDENCIES=$bam_hard_clip_17_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.E4.4b382ec6a1cc578d0d31c8dfbae8b4d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.E4.4b382ec6a1cc578d0d31c8dfbae8b4d1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E4 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E4 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/E4/E4.sorted.mdup.hardClip.bam
cuffquant.E4.4b382ec6a1cc578d0d31c8dfbae8b4d1.mugqic.done
)
cuffquant_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_18_JOB_ID: cuffquant.E6
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.E6
JOB_DEPENDENCIES=$bam_hard_clip_18_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.E6.35c394c2cfb06bc2284331a327cda19d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.E6.35c394c2cfb06bc2284331a327cda19d.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E6 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E6 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/E6/E6.sorted.mdup.hardClip.bam
cuffquant.E6.35c394c2cfb06bc2284331a327cda19d.mugqic.done
)
cuffquant_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_19_JOB_ID: cuffquant.E8
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.E8
JOB_DEPENDENCIES=$bam_hard_clip_19_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.E8.405150b30b9c98ca9d0f3c3955351039.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.E8.405150b30b9c98ca9d0f3c3955351039.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E8 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E8 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/E8/E8.sorted.mdup.hardClip.bam
cuffquant.E8.405150b30b9c98ca9d0f3c3955351039.mugqic.done
)
cuffquant_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_20_JOB_ID: cuffquant.E9
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.E9
JOB_DEPENDENCIES=$bam_hard_clip_20_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.E9.90183b760fedcbf73081c738de1b3cbd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.E9.90183b760fedcbf73081c738de1b3cbd.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/E9 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/E9 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  alignment/E9/E9.sorted.mdup.hardClip.bam
cuffquant.E9.90183b760fedcbf73081c738de1b3cbd.mugqic.done
)
cuffquant_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffquant_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffdiff
#-------------------------------------------------------------------------------
STEP=cuffdiff
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffdiff_1_JOB_ID: cuffdiff.Omega-6-AA_vs_Omega-9
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Omega-6-AA_vs_Omega-9
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Omega-6-AA_vs_Omega-9.f0bafcd22c6bef1624317d4e779d60a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Omega-6-AA_vs_Omega-9.f0bafcd22c6bef1624317d4e779d60a8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Omega-6-AA_vs_Omega-9 && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Omega-6-AA_vs_Omega-9 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/B11/abundances.cxb,cufflinks/B12/abundances.cxb,cufflinks/B1/abundances.cxb,cufflinks/B6/abundances.cxb,cufflinks/B8/abundances.cxb \
  cufflinks/C11/abundances.cxb,cufflinks/C12/abundances.cxb,cufflinks/C4/abundances.cxb,cufflinks/C5/abundances.cxb,cufflinks/C9/abundances.cxb
cuffdiff.Omega-6-AA_vs_Omega-9.f0bafcd22c6bef1624317d4e779d60a8.mugqic.done
)
cuffdiff_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffdiff_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_2_JOB_ID: cuffdiff.Omega-3-DHA_vs_Omega-9
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Omega-3-DHA_vs_Omega-9
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Omega-3-DHA_vs_Omega-9.3a239c1875377974f9cfd394c9dece04.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Omega-3-DHA_vs_Omega-9.3a239c1875377974f9cfd394c9dece04.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Omega-3-DHA_vs_Omega-9 && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Omega-3-DHA_vs_Omega-9 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/B11/abundances.cxb,cufflinks/B12/abundances.cxb,cufflinks/B1/abundances.cxb,cufflinks/B6/abundances.cxb,cufflinks/B8/abundances.cxb \
  cufflinks/D12/abundances.cxb,cufflinks/D2/abundances.cxb,cufflinks/D3/abundances.cxb,cufflinks/D4/abundances.cxb,cufflinks/D8/abundances.cxb
cuffdiff.Omega-3-DHA_vs_Omega-9.3a239c1875377974f9cfd394c9dece04.mugqic.done
)
cuffdiff_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffdiff_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_3_JOB_ID: cuffdiff.Omega-3-EPA_vs_Omega-9
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Omega-3-EPA_vs_Omega-9
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID:$cuffquant_19_JOB_ID:$cuffquant_20_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Omega-3-EPA_vs_Omega-9.fa94bff82c5443311bc74607634de043.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Omega-3-EPA_vs_Omega-9.fa94bff82c5443311bc74607634de043.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Omega-3-EPA_vs_Omega-9 && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Omega-3-EPA_vs_Omega-9 \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/B11/abundances.cxb,cufflinks/B12/abundances.cxb,cufflinks/B1/abundances.cxb,cufflinks/B6/abundances.cxb,cufflinks/B8/abundances.cxb \
  cufflinks/E3/abundances.cxb,cufflinks/E4/abundances.cxb,cufflinks/E6/abundances.cxb,cufflinks/E8/abundances.cxb,cufflinks/E9/abundances.cxb
cuffdiff.Omega-3-EPA_vs_Omega-9.fa94bff82c5443311bc74607634de043.mugqic.done
)
cuffdiff_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffdiff_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_4_JOB_ID: cuffdiff.Omega-3-DHA_vs_Omega-6-AA
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Omega-3-DHA_vs_Omega-6-AA
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Omega-3-DHA_vs_Omega-6-AA.0d0e58c0e0e4b498a6f6785251f95784.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Omega-3-DHA_vs_Omega-6-AA.0d0e58c0e0e4b498a6f6785251f95784.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Omega-3-DHA_vs_Omega-6-AA && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Omega-3-DHA_vs_Omega-6-AA \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/C11/abundances.cxb,cufflinks/C12/abundances.cxb,cufflinks/C4/abundances.cxb,cufflinks/C5/abundances.cxb,cufflinks/C9/abundances.cxb \
  cufflinks/D12/abundances.cxb,cufflinks/D2/abundances.cxb,cufflinks/D3/abundances.cxb,cufflinks/D4/abundances.cxb,cufflinks/D8/abundances.cxb
cuffdiff.Omega-3-DHA_vs_Omega-6-AA.0d0e58c0e0e4b498a6f6785251f95784.mugqic.done
)
cuffdiff_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffdiff_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_5_JOB_ID: cuffdiff.Omega-3-EPA_vs_Omega-6-AA
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Omega-3-EPA_vs_Omega-6-AA
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID:$cuffquant_19_JOB_ID:$cuffquant_20_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Omega-3-EPA_vs_Omega-6-AA.0ebd02b9428db380a4dac8890dd55ac1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Omega-3-EPA_vs_Omega-6-AA.0ebd02b9428db380a4dac8890dd55ac1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Omega-3-EPA_vs_Omega-6-AA && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Omega-3-EPA_vs_Omega-6-AA \
  --num-threads 12 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/C11/abundances.cxb,cufflinks/C12/abundances.cxb,cufflinks/C4/abundances.cxb,cufflinks/C5/abundances.cxb,cufflinks/C9/abundances.cxb \
  cufflinks/E3/abundances.cxb,cufflinks/E4/abundances.cxb,cufflinks/E6/abundances.cxb,cufflinks/E8/abundances.cxb,cufflinks/E9/abundances.cxb
cuffdiff.Omega-3-EPA_vs_Omega-6-AA.0ebd02b9428db380a4dac8890dd55ac1.mugqic.done
)
cuffdiff_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffdiff_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffnorm
#-------------------------------------------------------------------------------
STEP=cuffnorm
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffnorm_1_JOB_ID: cuffnorm
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID:$cuffquant_19_JOB_ID:$cuffquant_20_JOB_ID
JOB_DONE=job_output/cuffnorm/cuffnorm.15a50db9d65dbb3344db870bfc5e7761.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm.15a50db9d65dbb3344db870bfc5e7761.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffnorm && \
cuffnorm -q  \
  --library-type fr-firststrand \
  --output-dir cuffnorm \
  --num-threads 12 \
  --labels B11,B12,B1,B6,B8,C11,C12,C4,C5,C9,D12,D2,D3,D4,D8,E3,E4,E6,E8,E9 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/B11/abundances.cxb \
  cufflinks/B12/abundances.cxb \
  cufflinks/B1/abundances.cxb \
  cufflinks/B6/abundances.cxb \
  cufflinks/B8/abundances.cxb \
  cufflinks/C11/abundances.cxb \
  cufflinks/C12/abundances.cxb \
  cufflinks/C4/abundances.cxb \
  cufflinks/C5/abundances.cxb \
  cufflinks/C9/abundances.cxb \
  cufflinks/D12/abundances.cxb \
  cufflinks/D2/abundances.cxb \
  cufflinks/D3/abundances.cxb \
  cufflinks/D4/abundances.cxb \
  cufflinks/D8/abundances.cxb \
  cufflinks/E3/abundances.cxb \
  cufflinks/E4/abundances.cxb \
  cufflinks/E6/abundances.cxb \
  cufflinks/E8/abundances.cxb \
  cufflinks/E9/abundances.cxb
cuffnorm.15a50db9d65dbb3344db870bfc5e7761.mugqic.done
)
cuffnorm_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$cuffnorm_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: fpkm_correlation_matrix
#-------------------------------------------------------------------------------
STEP=fpkm_correlation_matrix
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_1_JOB_ID: fpkm_correlation_matrix_transcript
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_transcript
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_transcript.2d56bdad701a899cf3a678871c049eff.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_transcript.2d56bdad701a899cf3a678871c049eff.mugqic.done'
module load mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p metrics && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/isoforms.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/transcripts_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_transcript.2d56bdad701a899cf3a678871c049eff.mugqic.done
)
fpkm_correlation_matrix_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$fpkm_correlation_matrix_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_2_JOB_ID: fpkm_correlation_matrix_gene
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_gene
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_gene.ed1ff067871efb85a076c7f045782017.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_gene.ed1ff067871efb85a076c7f045782017.mugqic.done'
module load mugqic/R_Bioconductor/3.2.3_3.2 && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/genes.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/gene_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_gene.ed1ff067871efb85a076c7f045782017.mugqic.done
)
fpkm_correlation_matrix_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$fpkm_correlation_matrix_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
STEP=gq_seq_utils_exploratory_analysis_rnaseq
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID:$cuffnorm_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq.9890618df0f9bb9ad6794fbb0e8e508e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq.9890618df0f9bb9ad6794fbb0e8e508e.mugqic.done'
module load mugqic/R_Bioconductor/3.2.3_3.2 mugqic/mugqic_R_packages/1.0.4 && \
mkdir -p exploratory && \
R --no-save --no-restore <<-EOF
suppressPackageStartupMessages(library(gqSeqUtils))

exploratoryAnalysisRNAseq(htseq.counts.path="DGE/rawCountMatrix.csv", cuffnorm.fpkms.dir="cuffnorm", genes.path="/cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/annotations/Mus_musculus.mm10.UCSC2012-02-07.genes.tsv", output.dir="exploratory")
desc = readRDS(file.path("exploratory","index.RData"))
write.table(desc,file=file.path("exploratory","index.tsv"),sep='	',quote=F,col.names=T,row.names=F)
print("done.")

EOF
gq_seq_utils_exploratory_analysis_rnaseq.9890618df0f9bb9ad6794fbb0e8e508e.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=00:30:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq_report
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq_report
JOB_DEPENDENCIES=$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq_report.27046610941f9c990a2fc1cc886ba7a0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq_report.27046610941f9c990a2fc1cc886ba7a0.mugqic.done'
module load mugqic/R_Bioconductor/3.2.3_3.2 mugqic/pandoc/1.15.2 && \
R --no-save --no-restore <<-'EOF'
report_dir="report";
input_rmarkdown_file = '/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.gq_seq_utils_exploratory_analysis_rnaseq.Rmd'
render_output_dir    = 'report'
rmarkdown_file       = basename(input_rmarkdown_file) # honoring a different WD that location of Rmd file in knitr is problematic
file.copy(from = input_rmarkdown_file, to = rmarkdown_file, overwrite = T)
rmarkdown::render(input = rmarkdown_file, output_format = c("html_document","md_document"), output_dir = render_output_dir  )
file.remove(rmarkdown_file)
EOF
gq_seq_utils_exploratory_analysis_rnaseq_report.27046610941f9c990a2fc1cc886ba7a0.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID: cuffnorm_report
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm_report
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/cuffnorm_report.dc98b6d5015f02c7f5eb5186ccf442d8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm_report.dc98b6d5015f02c7f5eb5186ccf442d8.mugqic.done'
mkdir -p report && \
zip -r report/cuffAnalysis.zip cufflinks/ cuffdiff/ cuffnorm/ && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/RnaSeq.cuffnorm.md \
  report/RnaSeq.cuffnorm.md
cuffnorm_report.dc98b6d5015f02c7f5eb5186ccf442d8.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: differential_expression
#-------------------------------------------------------------------------------
STEP=differential_expression
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: differential_expression_1_JOB_ID: differential_expression
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/differential_expression/differential_expression.5937accae4126567f237d9ea7f64e329.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression.5937accae4126567f237d9ea7f64e329.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 mugqic/R_Bioconductor/3.1.2_3.0 && \
mkdir -p DGE && \
Rscript $R_TOOLS/edger.R \
  -d ../../../../project/vyt-470-aa/Omega3/input/Design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE && \
Rscript $R_TOOLS/deseq.R \
  -d ../../../../project/vyt-470-aa/Omega3/input/Design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE
differential_expression.5937accae4126567f237d9ea7f64e329.mugqic.done
)
differential_expression_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=10:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=1700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$differential_expression_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=lg-1r14-n04&ip=10.241.129.4&pipeline=RnaSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,star,picard_merge_sam_files,picard_sort_sam,picard_mark_duplicates,picard_rna_metrics,estimate_ribosomal_rna,bam_hard_clip,rnaseqc,wiggle,raw_counts,raw_counts_metrics,cufflinks,cuffmerge,cuffquant,cuffdiff,cuffnorm,fpkm_correlation_matrix,gq_seq_utils_exploratory_analysis_rnaseq,differential_expression&samples=20" --quiet --output-document=/dev/null

